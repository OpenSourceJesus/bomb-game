#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.Tilemaps;
using System.Collections.Generic;

namespace BombGame
{
	[ExecuteInEditMode]
	public class MakeSpriteShapesFromTilemapTilesOfType : EditorScript
	{
		public Tilemap tilemap;
		public TileBase tileType;
		public _SpriteShapeController spriteShapeControllerPrefab;
		public float randomizePosition;
		public List<_SpriteShapeController> previousSpriteShapeControllers = new List<_SpriteShapeController>();

		public override void Do ()
		{
			if (tilemap == null)
				tilemap = GetComponent<Tilemap>();
			foreach (_SpriteShapeController spriteShapeController in previousSpriteShapeControllers)
			{
				if (spriteShapeController != null)
					GameManager.DestroyOnNextEditorUpdate (spriteShapeController.gameObject);
			}
			previousSpriteShapeControllers.Clear();
			Vector2 tileSize = tilemap.CellToWorld(Vector3Int.one) - tilemap.CellToWorld(Vector3Int.zero);
			List<Shape2D> shapes = new List<Shape2D>();
			foreach (Vector3Int cellPosition in tilemap.cellBounds.allPositionsWithin)
			{
				if (tilemap.GetTile(cellPosition) == tileType)
				{
					Rect2D rect2D = new Rect2D(tilemap.GetCellCenterWorld(cellPosition), tileSize);
					for (int i = 0; i < 4; i ++)
						rect2D.corners[i] += Random.insideUnitCircle * randomizePosition;
					rect2D.SetEdgesForPolygon ();
					shapes.Add(rect2D);
					int shapeCount = shapes.Count;
					for (int i = 0; i < shapeCount - 1; i ++)
					{
						Shape2D shape = shapes[i];
						Vector2[] sameCorners = shape.corners.GetPointsWithinRange(rect2D.corners, randomizePosition * 2);
						if (sameCorners.Length > 0)
						{
							for (int i2 = 0; i2 < 4; i2 ++)
							{
								Vector2 corner = rect2D.corners[i2];
								if (!sameCorners.Contains(corner))
									shape = shape.InsertCornerBetweenClosestEdgeForPolygon(corner);
							}
							shapes.Remove(rect2D);
							for (int i2 = 0; i2 < shapes.Count; i2 ++)
							{
								if (i != i2)
								{
									Shape2D shape2 = shapes[i2];
									Vector2[] sameCorners2 = shape2.corners.GetPointsWithinRange(shape.corners, randomizePosition * 2);
									if (sameCorners2.Length > 0)
									{
										for (int i3 = 0; i3 < shape2.corners.Length; i3 ++)
										{
											Vector2 corner = shape2.corners[i3];
											if (!sameCorners2.Contains(corner))
												shape = shape.InsertCornerBetweenClosestEdgeForPolygon(corner);
										}
										shapes.RemoveAt(i2);
										i2 --;
									}
								}
							}
							shapes[i] = shape;
							break;
						}
					}
				}
			}
			foreach (Shape2D shape in shapes)
			{
				_SpriteShapeController spriteShapeController = (_SpriteShapeController) PrefabUtility.InstantiatePrefab(spriteShapeControllerPrefab);
				shape.ToSpline (spriteShapeController.spline);
				previousSpriteShapeControllers.Add(spriteShapeController);
			}
		}
	}
}
#else
namespace BombGame
{
	public class MakeSpriteShapesFromTilemapTilesOfType : EditorScript
	{
	}
}
#endif