using System;
using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace BombGame
{
	public class Player : SingletonUpdateWhileEnabled<Player>, IDestructable
	{
		public float Hp
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}
		public uint MaxHp
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}
		public uint id;
		public Transform trs;
		public Rigidbody2D rigid;
		public PolygonCollider2D polygonCollider;
		public LinecastOffset groundLinecastOffset;
		public LinecastOffset roofLinecastOffset;
		public LinecastOffset wallLinecastOffset;
		public LayerMask whatIsGround;
		public Rect ColliderRect
		{
			get
			{
				return polygonCollider.bounds.ToRect().SetToPositiveSize();
			}
		}
		[HideInInspector]
		public bool isGrounded;
		[HideInInspector]
		public bool isJumping;
		public SerializableDictionary<string, VelocityEffector_Vector2> velocityEffectors_Vector2Dict = new SerializableDictionary<string, VelocityEffector_Vector2>();
		public SerializableDictionary<string, VelocityEffector_float> velocityEffectors_floatDict = new SerializableDictionary<string, VelocityEffector_float>();
		public float maxXVelocityToBeStuck;
		public float raiseAmountToGetUnstuck;
		public float shootSpeed;
		public Transform shootSpawnTrs;
		public Renderer renderer;
		public LineRenderer aimingVisualizer;
		public Bomb[] bombPrefabs = new Bomb[0];
		public float reloadDuration;
		public GameObject[] currentBombIndicators = new GameObject[0];
		public bool invulnerable;
		public float minMoveDistanceForSync;
		public float timeIntervalForSync;
		[HideInInspector]
		public Match.Team owner;
		[HideInInspector]
		public int currentBombIndex;
		[HideInInspector]
		public Bomb currentBomb;
		public Transform uiTrs;
		public GameObject invulnerableIndicator;
		public LayerMask whatBombsCollideWith;
		[HideInInspector]
		public float multiplyVelocityChanges;
		public FloatRange angleRangeToSlide;
		public float minRangeToUseFullShootSpeed;
		public float divideJoystickRange;
		public bool canHurtSelf;
		[HideInInspector]
		public Vector2 aimVector;
		[HideInInspector]
		public float reloadTimer;
		[HideInInspector]
		public Dictionary<string, Bomb> armedBombNamesDict = new Dictionary<string, Bomb>();
		public Vector2 SavedPosition
		{
			get
			{
				return PlayerPrefsExtensions.GetVector2("Position", trs.position);
			}
			set
			{
				PlayerPrefsExtensions.SetVector2 ("Position", value);
			}
		}
		public static float timeSinceDeathFromLevelLoad;
		protected RaycastHit2D[] groundHits = new RaycastHit2D[2];
		protected LayerMask whatICollideWith;
		Vector2 syncedPosition;
		float timeOfLastSync;
		bool isHittingWall;
		Vector2 previousVelocity;
		float previousXPosition;
		bool shootInput;
		bool previousShootInput;
		bool isUsingTimedBomb;

		public override void Awake ()
		{
			base.Awake ();
			if (Match.instance == null)
			{
				uiTrs.SetParent(null);
				if (Survival.instance == null && _SceneManager.CurrentScene.name != "Survival")
					trs.position = SavedPosition;
			}
			multiplyVelocityChanges = 1;
			currentBomb = bombPrefabs[0];
			currentBombIndex = 0;
			currentBombIndicators[currentBombIndex].SetActive(currentBomb.PlayerOwns);
			whatICollideWith = Physics2D.GetLayerCollisionMask(gameObject.layer);
			whatICollideWith = whatICollideWith.Remove(LayerMask.LayerToName(currentBomb.gameObject.layer));
			velocityEffectors_Vector2Dict.Init ();
			velocityEffectors_floatDict.Init ();
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			shootInput = InputManager.ShootInput;
			HandleMovement ();
			HandleSliding ();
			HandleJumping ();
			HandleVelocity ();
			HandleSwitchBombs ();
			HandleAiming ();
			HandleShooting ();
			previousShootInput = shootInput;
		}

		public void HandleMovement ()
		{
			float moveInput = InputManager.MoveInput;
			Move (moveInput);
			if (moveInput != 0)
			{
				trs.localScale = trs.localScale.SetX(Mathf.Sign(moveInput) * Mathf.Abs(trs.localScale.x));
				foreach (KeyValuePair<string, Bomb> keyValuePair in armedBombNamesDict)
					keyValuePair.Value.trs.localScale = keyValuePair.Value.trs.localScale.SetX(Mathf.Sign(moveInput) * Mathf.Abs(keyValuePair.Value.trs.localScale.x));
			}
			if (((Vector2) trs.position - syncedPosition).sqrMagnitude >= minMoveDistanceForSync || (Time.time - timeOfLastSync >= timeIntervalForSync && syncedPosition != (Vector2) trs.position))
			{
				timeOfLastSync = Time.time;
				syncedPosition = trs.position;
				if (NetworkManager.connection != null)
					NetworkManager.connection.Send("Move Player", syncedPosition.x, syncedPosition.y);
			}
		}

		public void Move (float speed)
		{
			velocityEffectors_Vector2Dict["Movement"].value.x = speed * velocityEffectors_floatDict["Move Speed"].value * multiplyVelocityChanges;
		}

		void HandleSliding ()
		{
			Vector2 bottomLeftColliderCorner = ColliderRect.min;
			Vector2 bottomRightColliderCorner = new Vector2(ColliderRect.xMax, ColliderRect.yMin);
			Vector2 offsetPhysicsQuery = Vector2.down * groundLinecastOffset.positionChangeAlongPerpendicular;
			ContactFilter2D contactFilter = new ContactFilter2D();
			contactFilter.layerMask = whatIsGround;
			contactFilter.useLayerMask = true;
			RaycastHit2D hit = Physics2D.Linecast(bottomLeftColliderCorner + offsetPhysicsQuery, bottomRightColliderCorner + offsetPhysicsQuery, whatICollideWith);
			if (hit.collider != null)
			{
				float angle = Vector2.Angle(hit.normal, Vector2.down);
				if (angleRangeToSlide.Contains(angle, false))
					velocityEffectors_Vector2Dict["Slide"].value = hit.normal.RotateTo(Vector2.down, 90) * (Physics2D.gravity.magnitude * Time.deltaTime * velocityEffectors_floatDict["Gravity Scale"].value + velocityEffectors_Vector2Dict["Slide"].value.magnitude);
				else
					velocityEffectors_Vector2Dict["Slide"].value = Vector2.zero;
			}
		}

		public void HandleJumping ()
		{
			if (InputManager.JumpInput && isGrounded && !isJumping)
				StartJump ();
			else if (isJumping)
			{
				if (!InputManager.JumpInput)
					StopJump ();
				if (rigid.velocity.y <= 0)
					isJumping = false;
			}
		}

		void HandleSwitchBombs ()
		{
			if (InputManager.UsingKeyboard)
			{
				if (Keyboard.current.zKey.wasPressedThisFrame)
				{
					SwtichToBomb (0);
					if (isUsingTimedBomb)
						SwtichToBomb (currentBombIndex + 1);
				}
				else if (Keyboard.current.xKey.wasPressedThisFrame)
				{
					SwtichToBomb (2);
					if (isUsingTimedBomb)
						SwtichToBomb (currentBombIndex + 1);
				}
				else if (Keyboard.current.cKey.wasPressedThisFrame)
				{
					SwtichToBomb (4);
					if (isUsingTimedBomb)
						SwtichToBomb (currentBombIndex + 1);
				}
				else if (Keyboard.current.vKey.wasPressedThisFrame)
				{
					SwtichToBomb (6);
					if (isUsingTimedBomb)
						SwtichToBomb (currentBombIndex + 1);
				}
				if (Keyboard.current.leftAltKey.wasPressedThisFrame)
				{
					isUsingTimedBomb = !isUsingTimedBomb;
					if (!isUsingTimedBomb)
						SwtichToBomb (currentBombIndex - 1);
					else
						SwtichToBomb (currentBombIndex + 1);
				}
			}
		}

		void SwtichToBomb (int bombIndex)
		{
			Bomb switchToBomb = bombPrefabs[bombIndex];
			if (switchToBomb.PlayerOwns)
			{
				currentBombIndicators[currentBombIndex].SetActive(false);
				currentBombIndex = bombIndex;
				currentBomb = switchToBomb;
				currentBombIndicators[currentBombIndex].SetActive(true);
			}
		}

		void HandleAiming ()
		{
			aimVector = new Vector2();
			if (InputManager.UsingGamepad)
				aimVector = Gamepad.current.rightStick.ReadValue() * shootSpeed / divideJoystickRange;
			else if (InputManager.UsingMouse)
				aimVector = GameCamera.Instance.camera.ScreenToWorldPoint(Mouse.current.position.ReadValue()) - trs.position;
			aimVector = Vector2.ClampMagnitude(aimVector / minRangeToUseFullShootSpeed, 1);
			Vector2 currentPosition = shootSpawnTrs.position;
			Vector2 currentVelocity = aimVector * shootSpeed + rigid.velocity;
			List<Vector3> positions = new List<Vector3>();
			positions.Add(currentPosition);
			while (GameCamera.Instance.viewRect.Contains(currentPosition))
			{
				currentVelocity += Physics2D.gravity * Time.fixedDeltaTime;
				currentVelocity *= 1f - Time.fixedDeltaTime * currentBomb.rigid.drag;
				currentPosition += currentVelocity * Time.fixedDeltaTime;
				RaycastHit2D hit = Physics2DExtensions.LinecastWithWidth(positions[positions.Count - 1], currentPosition, aimingVisualizer.startWidth, whatBombsCollideWith);
				if (hit.collider != null)
				{
					positions.Add(hit.point);
					break;
				}
				if (positions.Contains(currentPosition))
					break;
				positions.Add(currentPosition);
			}
			aimingVisualizer.positionCount = positions.Count;
			aimingVisualizer.SetPositions(positions.ToArray());
		}

		void HandleShooting ()
		{
			reloadTimer += Time.deltaTime;
			if (shootInput && reloadTimer >= reloadDuration)
				Shoot (aimVector * shootSpeed);
		}

		public Bomb Shoot (Vector2 shootVector)
		{
			if (!currentBomb.PlayerOwns)
				return null;
			Bomb bomb;
			if (!armedBombNamesDict.TryGetValue(currentBomb.name + "(Clone)", out bomb))
			{
				if (!currentBomb.canExplode && armedBombNamesDict.Count > 0)
					return null;
				bomb = Instantiate(currentBomb, shootSpawnTrs.position, Quaternion.identity);
				bomb.owner = this;
				Physics2D.IgnoreCollision(polygonCollider, bomb.collider, true);
				Collider2D[] hitPortalColliders = Physics2D.OverlapCircleAll(shootSpawnTrs.position, bomb.radius, LayerMask.GetMask("Portal"));
				for (int i = 0; i <hitPortalColliders.Length; i ++)
				{
					Collider2D hitPortalCollider = hitPortalColliders[i];
					bomb.startedInPortals.Add(hitPortalCollider.GetComponent<Portal>());
				}
				bomb.collider.enabled = true;
			}
			else if (previousShootInput)
				return null;
			if (bomb.canExplode)
			{
				reloadTimer = 0;
				if (bomb.trs != null)
				{
					bomb.trs.SetParent(null);
					bomb.rigid.simulated = true;
					bomb.rigid.velocity = shootVector + rigid.velocity;
				}
				armedBombNamesDict.Remove(bomb.name);
			}
			else
			{
				bomb.trs.SetParent(trs);
				bomb.canExplode = true;
				armedBombNamesDict.Add(bomb.name, bomb);
			}
			if (Match.localPlayer == this && NetworkManager.connection != null)
			{
				shootVector += rigid.velocity;
				NetworkManager.connection.Send("Shoot Bomb", (uint) currentBombIndex, shootVector.x, shootVector.y);
			}
			return bomb;
		}

		public void StartJump ()
		{
			velocityEffectors_Vector2Dict["Jump"].value.y = velocityEffectors_floatDict["Jump Speed"].value * multiplyVelocityChanges;
			isJumping = true;
		}

		public void StopJump ()
		{
			velocityEffectors_Vector2Dict["Jump"].value.y = 0;
			isJumping = false;
		}

		void HandleVelocity ()
		{
			SetIsGrounded ();
			SetIsHittingWall ();
			SetVelocityEffector (velocityEffectors_Vector2Dict["Falling"]);
			SetVelocityEffector (velocityEffectors_Vector2Dict["Jump"]);
			SetVelocityEffector (velocityEffectors_Vector2Dict["Wind"]);
			Vector2 velocity = Vector2.zero;
			for (int i = 0; i < velocityEffectors_Vector2Dict.Count; i ++)
				velocity += velocityEffectors_Vector2Dict.values[i].value;
			rigid.velocity = velocity;
			if (!isHittingWall && Mathf.Abs(velocity.x) > maxXVelocityToBeStuck && Mathf.Abs(trs.position.x - previousXPosition) <= maxXVelocityToBeStuck * Time.deltaTime)
			{
				if (isGrounded)
					rigid.position += Vector2.up * raiseAmountToGetUnstuck;
				else
					rigid.position += Vector2.left * Mathf.Sign(velocity.x) * raiseAmountToGetUnstuck;
			}
			previousXPosition = trs.position.x;
			previousVelocity = rigid.velocity;
		}

		public void SetVelocityEffector (VelocityEffector_Vector2 velocityEffector)
		{
			if (velocityEffector.value.y > 0)
			{
				Vector2 offsetPhysicsQuery = Vector2.up * roofLinecastOffset.positionChangeAlongPerpendicular;
				Vector2 topLeftColliderCorner = new Vector2(ColliderRect.xMin, ColliderRect.yMax);
				Vector2 topRightColliderCorner = ColliderRect.max;
				ContactFilter2D contactFilter = new ContactFilter2D();
				contactFilter.layerMask = whatICollideWith;
				contactFilter.useLayerMask = true;
				if (Physics2D.Linecast(topLeftColliderCorner + offsetPhysicsQuery + (Vector2.left * roofLinecastOffset.lengthChange1), topRightColliderCorner + offsetPhysicsQuery + (Vector2.right * roofLinecastOffset.lengthChange2), contactFilter, new RaycastHit2D[1]) > 0)
					velocityEffector.value.y = 0;
				// Debug.DrawLine(topLeftColliderCorner + offsetPhysicsQuery + (Vector2.left * roofLinecastOffset.lengthChange1), topRightColliderCorner + offsetPhysicsQuery + (Vector2.right * roofLinecastOffset.lengthChange2), Color.red, 0);
			}
			else if (velocityEffector.value.y < 0)
			{
				Vector2 offsetPhysicsQuery = Vector2.down * groundLinecastOffset.positionChangeAlongPerpendicular;
				Vector2 bottomLeftColliderCorner = ColliderRect.min;
				Vector2 bottomRightColliderCorner = new Vector2(ColliderRect.xMax, ColliderRect.yMin);
				ContactFilter2D contactFilter = new ContactFilter2D();
				contactFilter.layerMask = whatICollideWith;
				contactFilter.useLayerMask = true;
				if (Physics2D.Linecast(bottomLeftColliderCorner + offsetPhysicsQuery + (Vector2.left * groundLinecastOffset.lengthChange1), bottomRightColliderCorner + offsetPhysicsQuery + (Vector2.right * groundLinecastOffset.lengthChange2), contactFilter, new RaycastHit2D[1]) > 0)
					velocityEffector.value.y = 0;
				// Debug.DrawLine(bottomLeftColliderCorner + offsetPhysicsQuery + (Vector2.left * groundLinecastOffset.lengthChange1), bottomRightColliderCorner + offsetPhysicsQuery + (Vector2.right * groundLinecastOffset.lengthChange2), Color.red, 0);
			}
			float gravityScale;
			switch (velocityEffector.name)
			{
				case "Falling":
					if (isGrounded || rigid.velocity.y > 0)
						velocityEffector.value.y = 0;
					else if (rigid.velocity.y <= 0 && velocityEffectors_Vector2Dict["Jump"].value.y == 0)
						velocityEffector.value += Physics2D.gravity * Time.deltaTime * velocityEffectors_floatDict["Gravity Scale"].value;
					break;
				case "Jump":
					gravityScale = velocityEffectors_floatDict["Gravity Scale"].value;
					if (velocityEffector.value.y > -Physics2D.gravity.y * Time.deltaTime * gravityScale)
						velocityEffector.value += Physics2D.gravity * Time.deltaTime * gravityScale;
					else
					{
						if (!isGrounded && velocityEffectors_Vector2Dict["Falling"].value.y == 0)
							velocityEffectors_Vector2Dict["Falling"].value.y = Physics2D.gravity.y * Time.deltaTime * gravityScale + velocityEffector.value.y;
						velocityEffector.value.y = 0;
					}
					break;
				default:
					gravityScale = velocityEffectors_floatDict["Gravity Scale"].value;
					if (velocityEffector.value.y > -Physics2D.gravity.y * Time.deltaTime * gravityScale)
						velocityEffector.value += Physics2D.gravity * Time.deltaTime * gravityScale;
					else if (velocityEffector.value.y > 0)
						velocityEffector.value.y = 0;
					float slowDownSpeed = velocityEffectors_floatDict["Slow Down Speed"].value;
					if (velocityEffector.value.magnitude > slowDownSpeed * Time.deltaTime)
					{
						if (isHittingWall)
							velocityEffector.value.x = 0;
						float movement = velocityEffectors_Vector2Dict["Movement"].value.x * multiplyVelocityChanges;
						if (velocityEffector.value.x != 0 && movement != 0 && Mathf.Sign(movement) != Mathf.Sign(velocityEffector.value.x))
						{
							if (Mathf.Abs(velocityEffector.value.x) > Mathf.Abs(movement * Time.deltaTime))
								velocityEffector.value.x += movement * Time.deltaTime;
							else
								velocityEffector.value.x = 0;
						}
						float slowDownAmount = slowDownSpeed * Time.deltaTime;
						if (velocityEffector.value.sqrMagnitude > slowDownAmount * slowDownAmount)
							velocityEffector.value -= velocityEffector.value.normalized * slowDownAmount;
						else
							velocityEffector.value = Vector2.zero;
					}
					else
						velocityEffector.value = Vector2.zero;
					break;
			}
			velocityEffector.value *= 1f - Time.deltaTime * rigid.drag;
		}

		void SetIsGrounded ()
		{
			Vector2 bottomLeftColliderCorner = ColliderRect.min;
			Vector2 bottomRightColliderCorner = new Vector2(ColliderRect.xMax, ColliderRect.yMin);
			Vector2 offsetPhysicsQuery = Vector2.down * groundLinecastOffset.positionChangeAlongPerpendicular;
			ContactFilter2D contactFilter = new ContactFilter2D();
			contactFilter.layerMask = whatIsGround;
			contactFilter.useLayerMask = true;
			isGrounded = Physics2D.Linecast(bottomLeftColliderCorner + offsetPhysicsQuery + (Vector2.left * groundLinecastOffset.lengthChange1), bottomRightColliderCorner + offsetPhysicsQuery + (Vector2.right * groundLinecastOffset.lengthChange2), contactFilter, groundHits) > 0;
		}

		void SetIsHittingWall ()
		{
			isHittingWall = false;
			if (previousVelocity.x > 0)
			{
				Vector2 offsetPhysicsQuery = Vector2.right * wallLinecastOffset.positionChangeAlongPerpendicular;
				Vector2 bottomRightColliderCorner = new Vector2(ColliderRect.xMax, ColliderRect.yMin);
				Vector2 topRightColliderCorner = ColliderRect.max;
				ContactFilter2D contactFilter = new ContactFilter2D();
				contactFilter.layerMask = whatICollideWith;
				contactFilter.useLayerMask = true;
				if (Physics2D.Linecast(bottomRightColliderCorner + offsetPhysicsQuery + (Vector2.down * wallLinecastOffset.lengthChange1), topRightColliderCorner + offsetPhysicsQuery + (Vector2.up * wallLinecastOffset.lengthChange2), contactFilter, new RaycastHit2D[1]) > 0)
				{
					isHittingWall = true;
					foreach (KeyValuePair<string, VelocityEffector_Vector2> keyValuePair in velocityEffectors_Vector2Dict)
						velocityEffectors_Vector2Dict[keyValuePair.Key].value.x = Mathf.Min(0, velocityEffectors_Vector2Dict[keyValuePair.Key].value.x);
				}
				// Debug.DrawLine(bottomRightColliderCorner + offsetPhysicsQuery + (Vector2.down * wallLinecastOffset.lengthChange1), topRightColliderCorner + offsetPhysicsQuery + (Vector2.up * wallLinecastOffset.lengthChange2), Color.red, 0);
			}
			else if (previousVelocity.x < 0)
			{
				Vector2 offsetPhysicsQuery = Vector2.left * wallLinecastOffset.positionChangeAlongPerpendicular;
				Vector2 bottomLeftColliderCorner = ColliderRect.min;
				Vector2 topLeftColliderCorner = new Vector2(ColliderRect.xMin, ColliderRect.yMax);
				ContactFilter2D contactFilter = new ContactFilter2D();
				contactFilter.layerMask = whatICollideWith;
				contactFilter.useLayerMask = true;
				if (Physics2D.Linecast(bottomLeftColliderCorner + offsetPhysicsQuery + (Vector2.down * wallLinecastOffset.lengthChange1), topLeftColliderCorner + offsetPhysicsQuery + (Vector2.up * wallLinecastOffset.lengthChange2), contactFilter, new RaycastHit2D[1]) > 0)
				{
					isHittingWall = true;
					foreach (KeyValuePair<string, VelocityEffector_Vector2> keyValuePair in velocityEffectors_Vector2Dict)
						velocityEffectors_Vector2Dict[keyValuePair.Key].value.x = Mathf.Max(0, velocityEffectors_Vector2Dict[keyValuePair.Key].value.x);
				}
				// Debug.DrawLine(bottomLeftColliderCorner + offsetPhysicsQuery + (Vector2.down * wallLinecastOffset.lengthChange1), topLeftColliderCorner + offsetPhysicsQuery + (Vector2.up * wallLinecastOffset.lengthChange2), Color.red, 0);
			}

		}
		
		public void TakeDamage (float amount, Player attacker)
		{
			if (!invulnerable && (attacker != this || canHurtSelf))
				Death (attacker);
		}
		
		public void Death (Player killer)
		{
			invulnerable = true;
			if (Match.instance != null)
			{
				if (Match.localPlayer == this)
				{
					Match.instance.OnPlayerDied (this, killer);
					Destroy(uiTrs.gameObject);
				}
			}
			else
			{
				timeSinceDeathFromLevelLoad = Time.timeSinceLevelLoad;
				for (int i = 0; i < velocityEffectors_Vector2Dict.Count; i ++)
					velocityEffectors_Vector2Dict[velocityEffectors_Vector2Dict.keys[i]].value = Vector2.zero;
				rigid.velocity = Vector2.zero;
				Enemy.instances = FindObjectsOfType<Enemy>(true);
				for (int i = 0; i < Enemy.instances.Length; i ++)
				{
					Enemy enemy = Enemy.instances[i];
					enemy.rigid.velocity = Vector2.zero;
					enemy.trs.position = enemy.initPosition;
					enemy.trs.eulerAngles = Vector3.forward * enemy.initRotation;
					enemy.enabled = false;
					enemy.animator.enabled = false;
					enemy.animator.enabled = true;
					enemy.gameObject.SetActive(true);
				}
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					Bomb bomb = bullet as Bomb;
					if (bomb != null)
					{
						if (bomb.explosion != null)
							Destroy(bomb.explosion.gameObject);
						PoisonBomb poisonBomb = bomb as PoisonBomb;
						if (poisonBomb != null)
						{
							Destroy(poisonBomb.gameObject);
							Destroy(poisonBomb.gameObject);
						}
						else
						{
							RiftBomb riftBomb = bomb as RiftBomb;
							if (riftBomb != null)
							{
								if (riftBomb.markerTrs != null)
									Destroy(riftBomb.markerTrs.gameObject);
								if (RiftBomb.previousMarkerTrs != null)
									Destroy(RiftBomb.previousMarkerTrs.gameObject);
							}
						}
					}
					Destroy(bullet.gameObject);
					Bullet.instances.RemoveAt(i);
					i --;
				}
				for (int i = 0; i < PortalBomb.previousPortals.Count; i ++)
				{
					Portal portal = PortalBomb.previousPortals[i];
					Destroy(portal.gameObject);
				}
				PortalBomb.previousPortals.Clear();
				for (int i = 0; i < ShooterTrap.instances.Length; i ++)
				{
					ShooterTrap shooterTrap = ShooterTrap.instances[i];
					shooterTrap.animator.enabled = false;
					shooterTrap.animator.enabled = true;
					if (shooterTrap.gameObject.activeInHierarchy)
						shooterTrap.Start ();
				}
				for (int i = 0; i < ActivaterTile.instances.Length; i ++)
				{
					ActivaterTile activaterTile = ActivaterTile.instances[i];
					activaterTile.Reset ();
				}
				for (int i = 0; i < Toggleable.instances.Length; i ++)
				{
					Toggleable toggleable = Toggleable.instances[i];
					if (toggleable.active != toggleable.initActive)
						toggleable.Toggle ();
				}
				GameManager.instance.Awake ();
				armedBombNamesDict.Clear();
				if (Survival.instance == null)
					trs.position = SavedPosition;
				else
					Survival.instance.OnPlayerDeath ();
				GameCamera.instance.HandlePosition ();
				if (World.instance != null)
					World.instance.SetPieces (false);
				invulnerable = false;
  			}
		}

		[Serializable]
		public class VelocityEffector<T>
		{
			public string name;
			public T value;

			public VelocityEffector (string name, T value)
			{
				this.name = name;
				this.value = value;
			}
		}

		[Serializable]
		public class VelocityEffector_Vector2 : VelocityEffector<Vector2>
		{
			public VelocityEffector_Vector2 (string name, Vector2 value) : base (name, value)
			{
			}
		}

		[Serializable]
		public class VelocityEffector_float : VelocityEffector<float>
		{
			public VelocityEffector_float (string name, float value) : base (name, value)
			{
			}
		}

		[Serializable]
		public class LinecastOffset
		{
			public float positionChangeAlongPerpendicular;
			public float lengthChange1;
			public float lengthChange2;
		}
	}
}