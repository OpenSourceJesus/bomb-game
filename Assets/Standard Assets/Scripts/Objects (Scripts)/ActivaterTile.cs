using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class ActivaterTile : _Tile, IDestructable
	{
		public float Hp
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}
		public uint MaxHp
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}
		public AudioSource audioSource;
		public UnityEvent onActivate;
		public float delayAfterActivate;
		public UnityEvent onDelayAfterActivate;
		public _Text timerText;
		public static ActivaterTile[] instances = new ActivaterTile[0];
		bool isActivated;
		TimerUpdater timerUpdater;
		
		public void TakeDamage (float amount, Player attacker)
		{
			if (isActivated)
				return;
			isActivated = true;
			audioSource.Play();
			onActivate.Invoke();
			renderer.material.SetColor("_tint", Color.grey.SetAlpha(renderer.material.GetColor("_tint").a));
			timerUpdater = new TimerUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(timerUpdater);
		}
		
		public override void OnDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(timerUpdater);
			base.OnDestroy ();
		}

		public void Death (Player killer)
		{
		}

		public void Reset ()
		{
			renderer.material.SetColor("_tint", Color.white.SetAlpha(renderer.material.GetColor("_tint").a));
			isActivated = false;
			timerText.Text = "";
			GameManager.updatables = GameManager.updatables.Remove(timerUpdater);
		}

		class TimerUpdater : IUpdatable
		{
			ActivaterTile activaterTile;
			float timer;

			public TimerUpdater (ActivaterTile activaterTile)
			{
				this.activaterTile = activaterTile;
				timer = activaterTile.delayAfterActivate;
			}

			public void DoUpdate ()
			{
				timer -= Time.deltaTime;
				if (timer <= 0)
				{
					timer = Mathf.Infinity;
					activaterTile.renderer.material.SetColor("_tint", Color.white.SetAlpha(activaterTile.renderer.material.GetColor("_tint").a));
					activaterTile.isActivated = false;
					activaterTile.timerText.Text = "";
				}
				else if (timer == Mathf.Infinity)
				{
					if (!activaterTile.isActivated)
						activaterTile.onDelayAfterActivate.Invoke();
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
				else
					activaterTile.timerText.Text = timer.ToString("F1");
			}
		}
	}
}