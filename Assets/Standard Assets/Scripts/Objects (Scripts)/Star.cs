using Extensions;
using UnityEngine;

namespace BombGame
{
	public class Star : SingletonMonoBehaviour<Star>
	{
		public Transform trs;
		public bool isCollected;
		public AudioClip onTriggerEnterAudioClip;

		void OnTriggerEnter2D (Collider2D other)
		{
			isCollected = true;
			gameObject.SetActive(false);
			AudioManager.instance.MakeSoundEffect (onTriggerEnterAudioClip, trs.position);
		}
	}
}