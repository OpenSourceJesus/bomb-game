using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class ElectricMist : _Tile
	{
		public float damagePerSecond;
		public float timeTouchedTillCharged;
		public float timeTouchedTillUncharged;
		public Gradient gradient;
		[HideInInspector]
		public float timer;
		List<IDestructable> currentTouchedDestructables = new List<IDestructable>();
		TimerUpdater timerUpdater;
		
		void OnTriggerEnter2D (Collider2D other)
		{
			currentTouchedDestructables.Add(other.GetComponent<IDestructable>());
			if (timer == 0)
			{
				timerUpdater = new TimerUpdater(this);
				GameManager.updatables = GameManager.updatables.Add(timerUpdater);
			}
		}
		
		void OnTriggerExit2D (Collider2D other)
		{
			currentTouchedDestructables.Remove(other.GetComponent<IDestructable>());
		}
		
		public override void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			GameManager.updatables = GameManager.updatables.Remove(timerUpdater);
			base.OnDestroy ();
		}

		class TimerUpdater : IUpdatable
		{
			ElectricMist electricMist;

			public TimerUpdater (ElectricMist electricMist)
			{
				this.electricMist = electricMist;
			}

			public void DoUpdate ()
			{
				if (electricMist.renderer == null)
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					return;
				}
				if (electricMist.currentTouchedDestructables.Count > 0)
				{
					electricMist.timer += 1f / electricMist.timeTouchedTillCharged * Time.deltaTime;
					if (electricMist.timer >= 1)
					{
						electricMist.timer = 1;
						for (int i = 0; i < electricMist.currentTouchedDestructables.Count; i ++)
						{
							IDestructable currentTouchedDestructable = electricMist.currentTouchedDestructables[i];
							if (currentTouchedDestructable != null)
								currentTouchedDestructable.TakeDamage (electricMist.damagePerSecond * Time.deltaTime, null);
						}
					}
				}
				else
				{
					electricMist.timer -= 1f / electricMist.timeTouchedTillUncharged * Time.deltaTime;
					if (electricMist.timer <= 0)
					{
						electricMist.timer = 0;
						GameManager.updatables = GameManager.updatables.Remove(this);
					}
				}
				electricMist.renderer.material.SetColor("_tint", electricMist.gradient.Evaluate(electricMist.timer));
			}
		}
	}
}