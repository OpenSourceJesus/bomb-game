using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace BombGame
{
	public class PortalBomb : Bomb
	{
		public float portalDuration;
		public Portal portal;
		public static List<Portal> previousPortals = new List<Portal>();

		public override void DoUpdate ()
		{
			if (explosion.trs != null && explosion.trs.localScale.x < explodeSize)
			{
				explosion.trs.localScale += Vector3.one * explodeSize * Time.deltaTime / explodeDuration;
				explosion.killZoneMaterial.SetFloat("_growNoiseTime", Time.time - explodeTime);
				explosion.windZoneMaterial.SetFloat("_growNoiseTime", Time.time - explodeTime);
			}
			else
			{
				if (previousPortals.Count > 0)
				{
					Portal previousPortal;
					if (previousPortals.Count > 1)
					{
						previousPortal = previousPortals[1];
						if (previousPortal != null)
							Destroy(previousPortal.gameObject);
						previousPortals.RemoveAt(1);
					}
					previousPortal = previousPortals[0];
					if (previousPortal != null)
					{
						portal.otherPortal = previousPortal;
						previousPortal.otherPortal = portal;
					}
					else
						previousPortals.RemoveAt(0);
				}
				previousPortals.Insert(0, portal);
				if (portal.trs == null)
					return;
				portal.trs.SetParent(null);
				portal.trs.localScale = Vector3.one * explodeSize * explosion.minRadiusFractionToNotKill;
				portal.gameObject.SetActive(true);
				Destroy(portal.gameObject, portalDuration);
				GameManager.updatables = GameManager.updatables.Remove(this);
				Destroy(gameObject);
				Destroy(explosion.gameObject);
				Destroy(explosion.spriteMaskTrs.gameObject);
			}
		}
	}
}