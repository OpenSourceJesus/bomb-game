using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace BombGame
{
	public class RiftBomb : Bomb
	{
		public float openSpeed;
		public float openDuration;
		public float waitDuration;
		public Transform markerTrs;
		public LineRenderer lineRenderer;
		public Material[] materials = new Material[0];
		public float maxMarkerDistanceToUseCollisionNormal;
		public static LineSegment2D riftCenterLineSegment = new LineSegment2D();
		public static Vector2 riftVector;
		public static State riftState;
		public static uint explodedCount;
		public const uint GROW_RIFT_AMOUNT = 9999;
		float riftTimer;
		public static Transform previousMarkerTrs;

		public override void DoUpdate ()
		{
			if (explosion.trs != null && explosion.trs.localScale.x < explodeSize)
			{
				explosion.trs.localScale += Vector3.one * explodeSize * Time.deltaTime / explodeDuration;
				explosion.killZoneMaterial.SetFloat("_growNoiseTime", Time.time - explodeTime);
				explosion.windZoneMaterial.SetFloat("_growNoiseTime", Time.time - explodeTime);
			}
			else if (explosion != null)
			{
				Destroy(explosion.gameObject);
				Destroy(explosion.spriteMaskTrs.gameObject);
				if (riftState == State.Disabled)
				{
					explodedCount ++;
					markerTrs.gameObject.SetActive(true);
					markerTrs.SetParent(null);
					if (explodedCount % 2 == 0)
					{
						riftCenterLineSegment.end = trs.position;
						riftVector = riftCenterLineSegment.end - riftCenterLineSegment.start;
						riftVector = riftVector.normalized * openSpeed;
						LineSegment2D lineSegment = new LineSegment2D(riftCenterLineSegment.start, riftCenterLineSegment.end);
						riftCenterLineSegment = riftCenterLineSegment.GetPerpendicular();
						Vector2 riftCenterLineSegmentDirection = riftCenterLineSegment.GetDirection();
						riftCenterLineSegment.start -= riftCenterLineSegmentDirection * GROW_RIFT_AMOUNT / 2;
						riftCenterLineSegment.end += riftCenterLineSegmentDirection * GROW_RIFT_AMOUNT / 2;
						riftState = State.Opening;
						lineRenderer.SetPositions(new Vector3[] { riftCenterLineSegment.start, riftCenterLineSegment.end });
						lineRenderer.enabled = true;
						for (int i = 0; i < materials.Length; i ++)
						{
							Material material = materials[i];
							material.SetVector("_riftPoint1", lineSegment.start);
							material.SetVector("_riftPoint2", lineSegment.end);
							material.SetFloat("_riftSpeed", openSpeed);
						}
						for (int i = 0; i < AffectedByRift.instances.Count; i ++)
						{
							AffectedByRift affectedByRift = AffectedByRift.instances[i];
							affectedByRift.Enable ();
						}
						GameCamera.instance.followPlayer = true;
					}
					else
					{
						previousMarkerTrs = markerTrs;
						riftCenterLineSegment.start = trs.position;
						GameManager.updatables = GameManager.updatables.Remove(this);
						Destroy(gameObject);
						return;
					}
				}
				else
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					Destroy(gameObject);
					return;
				}
			}
			if (riftState == State.Opening)
			{
				riftTimer += Time.deltaTime;
				if (riftTimer >= openDuration)
				{
					riftState = State.Waiting;
					riftTimer -= openDuration;
					for (int i = 0; i < materials.Length; i ++)
					{
						Material material = materials[i];
						material.SetFloat("_riftTimeOffset", openDuration);
					}
					for (int i = 0; i < AffectedByRift.instances.Count; i ++)
					{
						AffectedByRift affectedByRift = AffectedByRift.instances[i];
						affectedByRift.Disable ();
					}
				}
				else
				{
					for (int i = 0; i < materials.Length; i ++)
					{
						Material material = materials[i];
						material.SetFloat("_riftTimeOffset", riftTimer);
					}
				}
			}
			else if (riftState == State.Closing)
			{
				riftTimer += Time.deltaTime;
				if (riftTimer >= openDuration)
				{
					riftState = State.Disabled;
					for (int i = 0; i < materials.Length; i ++)
					{
						Material material = materials[i];
						material.SetVector("_riftPoint1", Vector2.right * 99999);
						material.SetVector("_riftPoint2", Vector2.zero);
						material.SetFloat("_riftTimeOffset", 0);
						material.SetFloat("_riftSpeed", 0);
					}
					List<AffectedByRift> affectedByRifts = new List<AffectedByRift>(AffectedByRift.instances);
					for (int i = 0; i < affectedByRifts.Count; i ++)
					{
						AffectedByRift affectedByRift = affectedByRifts[i];
						affectedByRift.Disable ();
					}
					Destroy(markerTrs.gameObject);
					Destroy(previousMarkerTrs.gameObject);
					GameCamera.instance.followPlayer = false;
					GameManager.updatables = GameManager.updatables.Remove(this);
					Destroy(gameObject);
				}
				else
				{
					for (int i = 0; i < materials.Length; i ++)
					{
						Material material = materials[i];
						material.SetFloat("_riftTimeOffset", openDuration - riftTimer);
					}
				}
			}
			else if (riftState == State.Waiting)
			{
				riftTimer += Time.deltaTime;
				if (riftTimer >= waitDuration)
				{
					riftTimer -= waitDuration;
					riftState = State.Closing;
					for (int i = 0; i < AffectedByRift.instances.Count; i ++)
					{
						AffectedByRift affectedByRift = AffectedByRift.instances[i];
						affectedByRift.Enable ();
					}
				}
			}
		}

		public override void OnCollisionEnter2D (Collision2D coll)
		{
			if (explodedCount % 2 == 1)
			{
				float markerDistanceSqr = (riftCenterLineSegment.start - (Vector2) trs.position).sqrMagnitude;
				if (markerDistanceSqr < maxMarkerDistanceToUseCollisionNormal * maxMarkerDistanceToUseCollisionNormal)
				{
					float markerDistance = Mathf.Sqrt(markerDistanceSqr);
					float moveAmount = (maxMarkerDistanceToUseCollisionNormal - markerDistance) / 2;
					Vector2 move = coll.GetContact(0).normal.Rotate90() * moveAmount;
					previousMarkerTrs.position -= (Vector3) move;
					riftCenterLineSegment.start -= move;
					trs.position += (Vector3) move;
				}
			}
			base.OnCollisionEnter2D (coll);
		}

		public override void OnDestroy ()
		{
			base.OnDestroy ();
			if (riftState != State.Disabled)
			{
				for (int i = 0; i < materials.Length; i ++)
				{
					Material material = materials[i];
					material.SetVector("_riftPoint1", Vector2.right * 99999);
					material.SetVector("_riftPoint2", Vector2.zero);
					material.SetFloat("_riftTimeOffset", 0);
					material.SetFloat("_riftSpeed", 0);
				}
			}
		}

		public enum State
		{
			Disabled,
			Opening,
			Waiting,
			Closing
		}
	}
}