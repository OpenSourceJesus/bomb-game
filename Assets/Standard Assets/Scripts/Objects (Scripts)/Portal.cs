using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class Portal : MonoBehaviour
	{
		public Transform trs;
		public Portal otherPortal;
		List<Transform> justTeleportedTransforms = new List<Transform>();

		void OnTriggerEnter2D (Collider2D other)
		{
			Bomb bomb = other.GetComponent<Bomb>();
			if (bomb != null && bomb.startedInPortals.Contains(this))
				return;
			Transform otherTrs = other.GetComponent<Transform>();
			if (otherPortal != null && !justTeleportedTransforms.Contains(otherTrs))
			{
				Vector2 offset = otherTrs.position - trs.position;
				otherTrs.position = (Vector2) otherPortal.trs.position + offset;
				otherPortal.justTeleportedTransforms.Add(otherTrs);
			}
		}

		void OnTriggerStay2D (Collider2D other)
		{
			if (otherPortal != null)
				OnTriggerEnter2D (other);
		}

		void OnTriggerExit2D (Collider2D other)
		{
			Bomb bomb = other.GetComponent<Bomb>();
			if (bomb != null && bomb.startedInPortals.Remove(this))
				return;
			else
				justTeleportedTransforms.Remove(other.GetComponent<Transform>());
		}
	}
}