using UnityEngine;
using System.Collections.Generic;

namespace BombGame
{
	public class ShooterTrap : MonoBehaviour
	{
		public BulletPatternEntry bulletPatternEntry;
		public Animator animator;
		public Collider2D collider;
		public float prewarmTime;
		public static ShooterTrap[] instances = new ShooterTrap[0];

		public void Start ()
		{
			if (WorldMap.isOpen)
				return;
			// ObjectPool.instance = ObjectPool.Instance;
			List<Bullet> bullets = new List<Bullet>();
			float animationLength = animator.GetCurrentAnimatorStateInfo(0).length;
			for (float time = Time.timeSinceLevelLoad - Player.timeSinceDeathFromLevelLoad; time <= prewarmTime; time += animationLength)
			{
				for (int i = 0; i < bullets.Count; i ++)
				{
					Bullet bullet = bullets[i];
					bullet.rigid.position += (Vector2) bullet.trs.up * bullet.moveSpeed * animationLength;
				}
				bullets.AddRange(bulletPatternEntry.Shoot(collider));
			}
		}

		public void Shoot ()
		{
			bulletPatternEntry.Shoot (collider);
		}
	}
}