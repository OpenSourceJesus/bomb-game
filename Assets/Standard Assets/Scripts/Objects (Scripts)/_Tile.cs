using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class _Tile : MonoBehaviour
	{
		public Transform trs;
		public Renderer renderer;
		public _Tile[] neighbors = new _Tile[0];
		public _Tile[] supportingTiles = new _Tile[0];
		public bool isSupportingTile;
		public TileParent tileParent;

		public virtual void OnEnable ()
		{
		}

		public virtual void OnDestroy ()
		{
			if (GameManager.paused || _SceneManager.isLoading || GameManager.isQuittingGame)
				return;
			for (int i = 0; i < neighbors.Length; i ++)
			{
				_Tile tile = neighbors[i];
				tile.neighbors = tile.neighbors.Remove(this);
			}
			if (tileParent != null)
			{
				// tileParent.UpdateRigidbody ();
				// tileParent.rigid.ResetCenterOfMass();
				// tileParent.rigid.ResetInertiaTensor();
				tileParent.rigid.mass -= renderer.bounds.GetVolume();
				ICollisionEnterHandler2D collisionEnterHandler = this as ICollisionEnterHandler2D;
				if (collisionEnterHandler != null)
					tileParent.collisionEnterHandlers = tileParent.collisionEnterHandlers.Remove(collisionEnterHandler);
			}
			for (int i = 0; i < neighbors.Length; i ++)
			{
				_Tile neighbor = neighbors[i];
				_Tile[] connectedTiles = _Tile.GetConnectedGroup(neighbor);
				bool shouldSplit = true;
				for (int i2 = 0; i2 < neighbor.supportingTiles.Length; i2 ++)
				{
					_Tile supportingTile = neighbor.supportingTiles[i2];
					if (connectedTiles.Contains(supportingTile) && supportingTile != null)
					{
						shouldSplit = false;
						break;
					}
				}
				if (shouldSplit)
					SplitTiles (connectedTiles);
			}
		}

		public static void SplitTiles (_Tile[] tiles)
		{
			TileParent tileParent = Instantiate(GameManager.instance.tileParentPrefab);
			tileParent.name += TileParent.lastUniqueId;
			TileParent.lastUniqueId ++;
			tileParent.rigid.mass = 0;
			for (int i = 0; i < tiles.Length; i ++)
			{
				_Tile tile = tiles[i];
				tile.trs.SetParent(tileParent.trs);
				if (tile.tileParent != null && tile.tileParent.trs.childCount == 0)
					Destroy(tile.tileParent.gameObject);
				tile.tileParent = tileParent;
				tileParent.rigid.mass += tile.renderer.bounds.GetVolume();
			}
			// tileParent.UpdateRigidbody ();
			// tileParent.rigid.ResetCenterOfMass();
			// tileParent.rigid.ResetInertiaTensor();
			tileParent.collisionEnterHandlers = tileParent.GetComponentsInChildren<ICollisionEnterHandler2D>();
		}

		public static bool AreNeighbors (_Tile tile, _Tile tile2, BoundsOffset? tileBoundsOffset = null, BoundsOffset? tile2BoundsOffset = null)
		{
			if (tileBoundsOffset == null)
				tileBoundsOffset = new BoundsOffset(Vector3.one / 2, Vector3.zero);
			if (tile2BoundsOffset == null)
				tile2BoundsOffset = new BoundsOffset(Vector3.one / 2, Vector3.zero);
			Vector3[] pointsInsideTile = tile.renderer.bounds.GetPointsInside(Vector3.one, (BoundsOffset) tileBoundsOffset);
			Vector3[] pointsInsideTile2 = tile2.renderer.bounds.GetPointsInside(Vector3.one, (BoundsOffset) tile2BoundsOffset);
			for (int i = 0; i < pointsInsideTile.Length; i ++)
			{
				Vector3 pointInsideTile = pointsInsideTile[i];
				pointInsideTile = pointInsideTile.Snap(Vector3.one / 2);
				for (int i2 = 0; i2 < pointsInsideTile2.Length; i2 ++)
				{
					Vector3 pointInsideTile2 = pointsInsideTile2[i2];
					pointInsideTile2 = pointInsideTile2.Snap(Vector3.one / 2);
					if ((pointInsideTile - pointInsideTile2).sqrMagnitude <= 1)
						return true;
				}
			}
			return false;
		}

		public static bool AreOverlapping (_Tile tile, _Tile tile2, BoundsOffset? tileBoundsOffset = null, BoundsOffset? tile2BoundsOffset = null)
		{
			if (tileBoundsOffset == null)
				tileBoundsOffset = new BoundsOffset(Vector3.one / 2, Vector3.zero);
			if (tile2BoundsOffset == null)
				tile2BoundsOffset = new BoundsOffset(Vector3.one / 2, Vector3.zero);
			Vector3[] pointsInsideTile = tile.renderer.bounds.GetPointsInside(Vector3.one, (BoundsOffset) tileBoundsOffset);
			Vector3[] pointsInsideTile2 = tile2.renderer.bounds.GetPointsInside(Vector3.one, (BoundsOffset) tile2BoundsOffset);
			for (int i = 0; i < pointsInsideTile.Length; i ++)
			{
				Vector3 pointInsideTile = pointsInsideTile[i];
				pointInsideTile = pointInsideTile.Snap(Vector3.one / 2);
				for (int i2 = 0; i2 < pointsInsideTile2.Length; i2 ++)
				{
					Vector3 pointInsideTile2 = pointsInsideTile2[i2];
					pointInsideTile2 = pointInsideTile2.Snap(Vector3.one / 2);
					if ((pointInsideTile - pointInsideTile2).sqrMagnitude <= .1f)
						return true;
				}
			}
			return false;
		}

		public static _Tile[] GetConnectedGroup (_Tile tile)
		{
			List<_Tile> output = new List<_Tile>();
			List<_Tile> checkedTiles = new List<_Tile>() { tile };
			List<_Tile> remainingTiles = new List<_Tile>() { tile };
			while (remainingTiles.Count > 0)
			{
				_Tile tile2 = remainingTiles[0];
				output.Add(tile2);
				for (int i = 0; i < tile2.neighbors.Length; i ++)
				{
					_Tile connectedTile = tile2.neighbors[i];
					if (!checkedTiles.Contains(connectedTile))
					{
						checkedTiles.Add(connectedTile);
						remainingTiles.Add(connectedTile);
					}
				}
				remainingTiles.RemoveAt(0);
			}
			return output.ToArray();
		}
	}
}