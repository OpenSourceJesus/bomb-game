﻿using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class _Button : Button, IUpdatable
	{
		public Image image;
		public FloatRange materialTimeOffsetRange;
		public _Text text;
		Color previousColor;

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			image.material = new Material(image.material);
			image.material.SetFloat("_timeOffset", materialTimeOffsetRange.Get(Random.value));
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			Color color;
			if (!interactable)
				color = colors.disabledColor;
			else if (IsPressed())
				color = colors.pressedColor;
			else if (IsHighlighted())
				color = colors.highlightedColor;
			else
				color = colors.normalColor;
			if (color != previousColor)
			{
				image.color = color;
				for (int i = 0; i < text.textCharacters.Length; i ++)
				{
					TextCharacter textCharacter = text.textCharacters[i];
					textCharacter.spriteRenderer.material.SetColor("_tint", color);
				}
			}
			previousColor = color;
		}

		void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}