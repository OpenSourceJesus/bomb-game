using UnityEngine;

namespace BombGame
{
	[DisallowMultipleComponent]
	public class EnemyGroup : MonoBehaviour
	{
		public Enemy[] enemies = new Enemy[0];
		[HideInInspector]
		public uint visionRangesPlayerIsIn;

#if UNITY_EDITOR
		public void OnValidate ()
		{
			enemies = GetComponentsInChildren<Enemy>();
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = enemies[i];
				enemy.enemyGroup = this;
			}
		}
#endif
	}
}