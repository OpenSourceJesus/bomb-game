﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class Hazard : Spawnable
	{
		public float damage;
		
		public virtual void OnCollisionEnter2D (Collision2D coll)
		{
			IDestructable destructable = coll.collider.GetComponent<IDestructable>();
			if (destructable != null)
				ApplyDamage (destructable, damage);
		}
		
		public virtual void ApplyDamage (IDestructable destructable, float amount)
		{
			destructable.TakeDamage (amount, null);
		}
	}
}