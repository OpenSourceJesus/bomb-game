using UnityEngine;

namespace BombGame
{
	public class ObjectInWorld : MonoBehaviour
	{
		public Transform trs;
		// [HideInInspector]
		public WorldPiece pieceIAmIn;
		// [HideInInspector]
		public ObjectInWorld duplicate;
		public ObjectInWorld[] objectsToLoadAndUnloadWithMe = new ObjectInWorld[0];
		public bool dontUnloadAfterLoad;
		public bool destroyIfAtLocalOrigin;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
		}
#endif

		void OnEnable ()
		{
			if (dontUnloadAfterLoad)
				trs.SetParent(null);
			if (destroyIfAtLocalOrigin && trs.localPosition == Vector3.zero)
				Destroy(gameObject);
		}
	}
}