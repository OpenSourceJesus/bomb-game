using UnityEngine;

namespace BombGame
{
	public class SoccerGoal : MonoBehaviour
	{
		public Match.Team owner;

		void OnCollisionEnter2D (Collision2D coll)
		{
			OnTriggerEnter2D (coll.collider);
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			if (other.GetComponent<Transform>() == Soccerball.instance.trs)
				Soccer.instance.OnGoalHit (this);
		}
	}
}