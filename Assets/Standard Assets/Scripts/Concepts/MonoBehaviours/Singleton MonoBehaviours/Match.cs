using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace BombGame
{
	public class Match : SingletonMonoBehaviour<Match>
	{
		public bool killsGiveScore;
		public Player playerPrefab;
		public Transform[] playerSpawnTransforms = new Transform[0];
		public float reviveDelay;
		public float makeVulnerableDelay;
		public uint minTimeWithNoScoreChangeToEndRound;
		public float multiplyReloadDurationsAtRoundEnd;
		public _Text waitingForMorePlayersText;
		public Canvas canvas;
		public static Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();
		public static Dictionary<uint, Team> playerTeamsDict = new Dictionary<uint, Team>();
		public static Player localPlayer;
		public static UIntRange playerCountRange;
		public static bool ranked;
		public static bool isPublic;
		public Transform reviveTimerBar;
		protected static uint localPlayerId;
		protected static byte maxScore;
		protected static bool hasEnded;
		protected const float MAX_SKILL_IN_CURRENT_SEASON_DIFFERENCE = .7f;
		static ReviveUpdater reviveUpdater;
		static int lastSpawnedAtSpawnPointIndexForLocalPlayer;
		static float reloadDuration;
		const string PRIVATE_GAME_INDICATOR = "Private: ";

		public override void Awake ()
		{
			base.Awake ();
			playersDict.Clear();
			playerTeamsDict.Clear();
			hasEnded = false;
			reloadDuration = playerPrefab.reloadDuration;
			NetworkManager.Disconnect ();
			NetworkManager.Connect (OnConnectSuccess, OnConnectFail);
		}

		void MakeDefaultMatch ()
		{
			playerCountRange = new UIntRange(2, 2);
			isPublic = true;
			ranked = true;
			maxScore = 10;
			MakeMatch ();
		}
		
		void MakeCustomMatch ()
		{
			playerCountRange = new UIntRange(MainMenu.MinPlayers, MainMenu.MaxPlayers);
			maxScore = 10;
			MakeMatch ();
		}

		void MakeMatch ()
		{
			if (isPublic)
				NetworkManager.client.Multiplayer.CreateJoinRoom(LocalUserInfo.username, name, true, GetMakeRoomData(), null, OnMakeOrJoinMatchSucess, NetworkManager.DisplayError);
			else
				NetworkManager.client.Multiplayer.CreateJoinRoom(PRIVATE_GAME_INDICATOR + MainMenu.MatchId, name, false, GetMakeRoomData(), null, OnMakeOrJoinMatchSucess, NetworkManager.DisplayError);
		}
		
		void ListMatches ()
		{
			isPublic = !MainMenu.isPrivate;
			ranked = isPublic && MainMenu.Ranked;
			NetworkManager.client.Multiplayer.ListRooms(name, new Dictionary<string, string>() { { "ranked", ranked.ToString() } }, 0, 0, OnListMatchesSuccess, NetworkManager.DisplayError);
		}

		void OnListMatchesSuccess (RoomInfo[] roomInfos)
		{
			for (int i = 0; i < roomInfos.Length; i ++)
			{
				RoomInfo roomInfo = roomInfos[i];
				if (roomInfo.OnlineUsers < uint.Parse(roomInfo.RoomData["maxPlayers"]) && (isPublic || roomInfo.Id == PRIVATE_GAME_INDICATOR + MainMenu.MatchId) && (!ranked || Mathf.Abs(LocalUserInfo.skillInCurrentSeasonDict[name] - float.Parse(roomInfo.RoomData[name + ".skillInCurrentSeason"])) <= MAX_SKILL_IN_CURRENT_SEASON_DIFFERENCE))
				{
					JoinMatch (roomInfo, OnMakeOrJoinMatchSucess, NetworkManager.DisplayError);
					return;
				}
			}
			MakeCustomMatch ();
		}

		void OnConnectSuccess (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			client.Multiplayer.UseSecureConnections = true;
			ListMatches ();
		}

		void OnConnectFail (PlayerIOError error)
		{
			GameManager.instance.DisplayNotification ("Error: " + error.ToString());
			NetworkManager.Connect (OnConnectSuccess, OnConnectFail);
		}

		public virtual Dictionary<string, string> GetMakeRoomData ()
		{
			Dictionary<string, string> output = new Dictionary<string, string>();
			output.Add("makeVulnerableDelay" , "" + makeVulnerableDelay);
			output.Add("minPlayers" , "" + playerCountRange.min);
			output.Add("maxPlayers" , "" + playerCountRange.max);
			output.Add("ranked" , ranked.ToString());
			output.Add("spawnPointCount" , "" + playerSpawnTransforms.Length);
			output.Add("minTimeWithNoScoreChangeToEndRound", "" + minTimeWithNoScoreChangeToEndRound);
			output.Add("maxScore", "" + maxScore);
			if (ranked)
				output.Add(name + ".skillInCurrentSeason", "" + LocalUserInfo.skillInCurrentSeasonDict[name]);
			return output;
		}

		Dictionary<string, string> GetJoinRoomData ()
		{
			return null;
		}

		void JoinMatch (RoomInfo roomInfo, Callback<Connection> onSuccess, Callback<PlayerIOError> onFail)
		{
			makeVulnerableDelay = float.Parse(roomInfo.RoomData["makeVulnerableDelay"]);
			playerCountRange = new UIntRange();
			playerCountRange.min = uint.Parse(roomInfo.RoomData["minPlayers"]);
			playerCountRange.max = uint.Parse(roomInfo.RoomData["maxPlayers"]);
			ranked = bool.Parse(roomInfo.RoomData["ranked"]);
			maxScore = byte.Parse(roomInfo.RoomData["maxScore"]);
			NetworkManager.client.Multiplayer.JoinRoom(roomInfo.Id, GetJoinRoomData(), onSuccess, onFail);
		}

		void OnMakeOrJoinMatchSucess (Connection connection)
		{
			NetworkManager.connection = connection;
			connection.OnMessage += OnMessage;
			connection.OnDisconnect += OnDisconnect;
			VoiceChat.instance.Init ();
		}

		void OnMessage (object sender, Message message)
		{
			switch (message.Type)
			{
				case "Spawn Player":
					OnSpawnPlayerMessage (message);
					break;
				case "Remove Player":
					OnRemovePlayerMessage (message);
					break;
				case "Move Player":
					OnMovePlayerMessage (message);
					break;
				case "Shoot Bomb":
					OnShootBombMessage (message);
					break;
				case "Kill Player":
					OnKillPlayerMessage (message);
					break;
				case "Revive Player":
					OnRevivePlayerMessage (message);
					break;
				case "Make Player Vulnerable":
					OnMakePlayerVulnerableMessage (message);
					break;
				case "End Round":
					OnEndRoundMessage (message);
					break;
			}
		}

		void OnDisconnect (object sender, string reason)
		{
			if (!hasEnded)
				End ();
		}

		public virtual Player OnSpawnPlayerMessage (Message message)
		{
			Player player = Instantiate(playerPrefab, playerSpawnTransforms[message.GetUInt(4)].position, Quaternion.identity);
			player.enabled = false;
			player.invulnerable = true;
			player.id = message.GetUInt(0);
			playersDict.Add(player.id, player);
			Color teamColor = new Color(message.GetFloat(1), message.GetFloat(2), message.GetFloat(3));
			player.owner = new Team(teamColor, 0, "");
			ApplyTeam (player);
			if (message.Count == 6)
			{
				localPlayer = player;
				localPlayerId = player.id;
				player.uiTrs.SetParent(null);
				GameCamera.instance = localPlayer.GetComponentInChildren<GameCamera>(true);
				GameCamera.instance.gameObject.SetActive(true);
				canvas.worldCamera = GameCamera.instance.camera;
			}
			else
				player.rigid.bodyType = RigidbodyType2D.Static;
 			Scoreboard.instance.AddEntry (player.owner);
			if (playersDict.Count >= playerCountRange.min)
			{
				waitingForMorePlayersText.gameObject.SetActive(false);
				foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
				{
					Player player2 = keyValuePair.Value;
					if (player2 != localPlayer)
						player2.GetComponentInChildren<GameCamera>(true).gameObject.SetActive(false);
				}
				if (localPlayer != null)
					localPlayer.enabled = true;
			}
			else
				UpdateWaitingForMorePlayersText ();
			return player;
		}

		protected void ApplyTeam (Player player)
		{
			Material material = new Material(player.renderer.sharedMaterial);
			material.SetColor("_tint", player.owner.color);
			player.renderer.sharedMaterial = material;
			playersDict[player.id] = player;
		}

		void OnRemovePlayerMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
				Destroy(player.gameObject);
			if (Scoreboard.instance != null)
				Scoreboard.instance.RemoveEntry (playerTeamsDict[playerId]);
			playersDict.Remove(playerId);
			playerTeamsDict.Remove(playerId);
			if (playersDict.Count < playerCountRange.min)
				UpdateWaitingForMorePlayersText ();
		}

		void OnMovePlayerMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
			{
				Vector2 position = new Vector2(message.GetFloat(1), message.GetFloat(2));
				player.trs.position = position;
			}
		}

		void OnShootBombMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
			{
				uint bombType = message.GetUInt(1);
				player.currentBombIndex = (int) bombType;
				player.currentBomb = player.bombPrefabs[bombType];
				Vector2 shootVector = new Vector2(message.GetFloat(2), message.GetFloat(3));
				player.Shoot (shootVector);
			}
		}

		void OnKillPlayerMessage (Message message)
		{
			uint killerId = message.GetUInt(0);
			Player killer = playersDict[killerId];
			uint victimId = message.GetUInt(1);
			Player victim = playersDict[victimId];
			OnPlayerDied (victim, killer);
		}

		void OnRevivePlayerMessage (Message message)
		{
			RevivePlayer (message.GetUInt(0), playerSpawnTransforms[message.GetUInt(1)]);
		}

		void OnMakePlayerVulnerableMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
				MakePlayerVulnerable (player);
		}
		
		void OnEndRoundMessage (Message message)
		{
			reloadDuration *= multiplyReloadDurationsAtRoundEnd;
			foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
				keyValuePair.Value.reloadDuration = reloadDuration;
		}

		public void OnPlayerDied (Player victim, Player killer)
		{
			Destroy(victim.gameObject);
			if (killer != victim)
			{
				if (killsGiveScore)
				{
					killer.owner.score ++;
					Scoreboard.instance.UpdateEntry (killer.owner);
				}
				if (ranked && localPlayer == killer)
					OnEnemyPlayerDeadInRanked ();
				else if (localPlayer == victim)
				{
					if (ranked)
						OnLocalPlayerDeadInRanked ();
					NetworkManager.connection.Send("Kill Player", killer.id);
					reviveUpdater = new ReviveUpdater(this, victim.id);
					GameManager.updatables = GameManager.updatables.Add(reviveUpdater);
				}
				if (killsGiveScore && !hasEnded && killer.owner.score >= maxScore)
				{
					if (ranked)
					{
						if (localPlayer == killer)
							OnLocalPlayerWonRanked ();
						else
							OnLocalPlayerLostRanked ();
					}
					End ();
				}
			}
			else
			{
				victim.owner.score --;
				Scoreboard.instance.UpdateEntry (victim.owner);
				if (localPlayer == victim)
				{
					if (ranked)
						OnLocalPlayerDeadInRanked ();
					NetworkManager.connection.Send("Kill Player", killer.id);
					reviveUpdater = new ReviveUpdater(this, victim.id);
					GameManager.updatables = GameManager.updatables.Add(reviveUpdater);
				}
			}
		}

		void OnLocalPlayerDeadInRanked ()
		{
			LocalUserInfo.deathsDict[name] ++;
			LocalUserInfo.deathsInCurrentSeasonDict[name] ++;
		}

		void OnEnemyPlayerDeadInRanked ()
		{
			LocalUserInfo.killsDict[name] ++;
			LocalUserInfo.killsInCurrentSeasonDict[name] ++;
		}

		void OnLocalPlayerWonRanked ()
		{
			LocalUserInfo.winsDict[name] ++;
			LocalUserInfo.winsInCurrentSeasonDict[name] ++;
			Player victim = playersDict[1 - localPlayerId];
			float skillChange = 1f - ((float) victim.owner.score / maxScore);
			LocalUserInfo.skillDict[name] += skillChange;
			LocalUserInfo.skillInCurrentSeasonDict[name] ++;
		}

		void OnLocalPlayerLostRanked ()
		{
			LocalUserInfo.lossesDict[name] ++;
			LocalUserInfo.lossesInCurrentSeasonDict[name] ++;
			float skillChange = 1f - ((float) localPlayer.owner.score / maxScore);
			LocalUserInfo.skillDict[name] -= skillChange;
			LocalUserInfo.skillInCurrentSeasonDict[name] -= skillChange;
		}

		void RevivePlayer (uint id, Transform spawnPoint)
		{
			Player player = Instantiate(playerPrefab, spawnPoint.position, Quaternion.identity);
			player.id = id;
			player.invulnerable = true;
			player.reloadDuration = reloadDuration;
			player.owner = playerTeamsDict[id];
			ApplyTeam (player);
			if (localPlayerId == id)
			{
				localPlayer = player;
				player.uiTrs.SetParent(null);
				GameCamera.instance = localPlayer.GetComponentInChildren<GameCamera>();
				canvas.worldCamera = GameCamera.instance.camera;
			}
			else
			{
				player.enabled = false;
				player.GetComponentInChildren<GameCamera>().gameObject.SetActive(false);
				player.rigid.bodyType = RigidbodyType2D.Static;
			}
		}

		void ReviveLocalPlayer (uint id)
		{
			lastSpawnedAtSpawnPointIndexForLocalPlayer = Random.Range(0, playerSpawnTransforms.Length);
			RevivePlayer (id, playerSpawnTransforms[lastSpawnedAtSpawnPointIndexForLocalPlayer]);
		}

		void MakePlayerVulnerable (Player player)
		{
			player.invulnerable = false;
			player.invulnerableIndicator.SetActive(false);
		}

		protected void End ()
		{
			hasEnded = true;
			_SceneManager.instance.LoadScene (0);
		}

		void UpdateWaitingForMorePlayersText ()
		{
			uint playersRemaining = playerCountRange.min - (uint) playersDict.Count;
			string waitingForMorePlayersString = "Waiting for " + playersRemaining + " more players";
			if (playersRemaining < 2)
				waitingForMorePlayersString = waitingForMorePlayersString.Remove(waitingForMorePlayersString.Length - 1);
			waitingForMorePlayersText.Text = waitingForMorePlayersString;
		}

		[Serializable]
		public class Team : Team<Player>
		{
			public Color color;
			public int score;

			public Team ()
			{
			}

			public Team (Color color, int score = 0, string name = "", params Player[] representatives) : base (name, representatives)
			{
				this.color = color;
				this.score = score;
			}
		}

		class ReviveUpdater : IUpdatable
		{
			Match match;
			uint playerId;
			float timer;

			public ReviveUpdater (Match match, uint playerId)
			{
				this.match = match;
				this.playerId = playerId;
				timer = match.reviveDelay;
				match.reviveTimerBar.gameObject.SetActive(true);
			}

			public void DoUpdate ()
			{
				timer -= Time.deltaTime;
				match.reviveTimerBar.localScale = match.reviveTimerBar.localScale.SetX(timer / match.reviveDelay);
				if (timer <= 0)
				{
					match.ReviveLocalPlayer (playerId);
					if (localPlayerId == playerId)
						NetworkManager.connection.Send("Revive Player", lastSpawnedAtSpawnPointIndexForLocalPlayer);
					match.reviveTimerBar.gameObject.SetActive(false);
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}