using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;

namespace BombGame
{
	public class MainMenu : SingletonMonoBehaviour<MainMenu>
	{
		public TMP_InputField minPlayersInputField;
		public TMP_InputField maxPlayersInputField;
		public Toggle rankedToggle;
		public TMP_InputField usernameInputField;
		public TMP_InputField passwordInputField;
		public Toggle rememberLoginInfoToggle;
		public TMP_InputField matchIdInputField;
		public Selectable[] registerAndLoginSelectables = new Selectable[0];
		public Button[] submitButtons = new Button[0];
		public GameObject[] deactivateGosOnLogin = new GameObject[0];
		public GameObject activateGoOnLogin;
		public static bool isPrivate;
		public static uint MinPlayers
		{
			get
			{
				return (uint) SaveAndLoadManager.GetInt("Min players", 1);
			}
			set
			{
				PlayerPrefs.SetInt("Min players", (int) value);
			}
		}
		public static uint MaxPlayers
		{
			get
			{
				return (uint) SaveAndLoadManager.GetInt("Max players", 4);
			}
			set
			{
				PlayerPrefs.SetInt("Max players", (int) value);
			}
		}
		public static bool Ranked
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Ranked", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Ranked", value);
			}
		}
		public static bool IsPrivate
		{
			get
			{
				return isPrivate;
			}
			set
			{
				isPrivate = value;
			}
		}
		public static string MatchId
		{
			get
			{
				return PlayerPrefs.GetString("Match id");
			}
			set
			{
				PlayerPrefs.SetString("Match id", value);
			}
		}
		public static string RememberedUsername
		{
			get
			{
				return PlayerPrefs.GetString("Username");
			}
			set
			{
				PlayerPrefs.SetString("Username", value);
			}
		}
		public static string RememberedPassword
		{
			get
			{
				return PlayerPrefs.GetString("Password");
			}
			set
			{
				PlayerPrefs.SetString("Password", value);
			}
		}
		public static bool RememberLoginInfo
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Remember login info", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Remember login info", value);
			}
		}
		static string password;
		static string[] usernames = new string[0];
		static DatabaseObject dbObj;

		public override void Awake ()
		{
			base.Awake ();
			minPlayersInputField.text = "" + MinPlayers;
			maxPlayersInputField.text = "" + MaxPlayers;
			rankedToggle.isOn = Ranked;
			if (RememberLoginInfo)
			{
				usernameInputField.text = RememberedUsername;
				passwordInputField.text = RememberedPassword;
			}
			rememberLoginInfoToggle.isOn = RememberLoginInfo;
			matchIdInputField.text = MatchId;
		}

		public void SetMinPlayers (TMP_InputField inputField)
		{
			string text = GetInputFieldText(inputField);
			MinPlayers = uint.Parse(text);
			if (MinPlayers == 0)
			{
				MinPlayers = 1;
				inputField.text = "" + MinPlayers;
			}
			else if (MinPlayers > MaxPlayers)
			{
				MinPlayers = MaxPlayers;
				inputField.text = "" + MinPlayers;
			}
			if (MinPlayers != 2)
				rankedToggle.isOn = false;
		}

		public void SetMaxPlayers (TMP_InputField inputField)
		{
			string text = GetInputFieldText(inputField);
			MaxPlayers = uint.Parse(text);
			if (MaxPlayers == 0)
			{
				MaxPlayers = 1;
				inputField.text = "" + MaxPlayers;
			}
			else if (MaxPlayers < MinPlayers)
			{
				MaxPlayers = MinPlayers;
				inputField.text = "" + MaxPlayers;
			}
			if (MaxPlayers != 2)
				rankedToggle.isOn = false;
		}

		public void SetRanked (bool ranked)
		{
			Ranked = ranked;
			if (ranked)
			{
				minPlayersInputField.text = "2";
				maxPlayersInputField.text = "2";
			}
		}

		public void SetUsername (string username)
		{
			if (RememberLoginInfo)
				RememberedUsername = username;
			LocalUserInfo.username = username;
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password);
			}
		}

		public void SetPassword (string password)
		{
			if (RememberLoginInfo)
				RememberedPassword = password;
			MainMenu.password = password;
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(LocalUserInfo.username) && !string.IsNullOrEmpty(password);
			}
		}

		public void SetRememberLoginInfo (bool remember)
		{
			RememberLoginInfo = remember;
			if (remember)
			{
				RememberedUsername = LocalUserInfo.username;
				RememberedPassword = password;
			}
		}

		public void Register ()
		{
			rankedToggle.interactable = false;
			rankedToggle.isOn = false;
			if (string.IsNullOrEmpty(LocalUserInfo.username))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty username!");
				return;
			}
			else if (string.IsNullOrEmpty(password))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty password!");
				return;
			}
			Logout ();
			NetworkManager.Connect (OnConnectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnConnectSuccess_Register (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			NetworkManager.client.BigDB.LoadOrCreate("Usernames", "usernames", OnLoadUsernamesDBObjectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnLoadUsernamesDBObjectSuccess_Register (DatabaseObject dbObj)
		{
			DatabaseArray usernamesDBArray = dbObj.GetArray("usernames");
			if (!usernamesDBArray.Contains(LocalUserInfo.username))
				usernamesDBArray.Add(LocalUserInfo.username);
			else
			{
				GameManager.instance.DisplayNotification ("An account with that username already exists!");
				return;
			}
			MainMenu.dbObj = dbObj;
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
		}

		void OnSaveDBObjectSuccess_Register ()
		{
			DatabaseObject dbObj2 = new DatabaseObject();
			dbObj2.Set("password", password);
			DatabaseArray dbArray = new DatabaseArray();
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
				dbArray.Add((float) 0);
			dbObj2.Set("skill", dbArray);
			dbObj2.Set("skillInCurrentSeason", dbArray);
			dbArray = new DatabaseArray();
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
				dbArray.Add((uint) 0);
			dbObj2.Set("wins", dbArray);
			dbObj2.Set("winsInCurrentSeason", dbArray);
			dbObj2.Set("losses", dbArray);
			dbObj2.Set("lossesInCurrentSeason", dbArray);
			dbObj2.Set("kills", dbArray);
			dbObj2.Set("killsInCurrentSeason", dbArray);
			dbObj2.Set("deaths", dbArray);
			dbObj2.Set("deathsInCurrentSeason", dbArray);
			dbObj2.Set("survivedTimeInSurvival", dbArray);
			dbObj2.Set("goalsMade", (uint) 0);
			dbObj2.Set("goalsMadeInCurrentSeason", (uint) 0);
			dbObj2.Set("goaledOn", (uint) 0);
			dbObj2.Set("goaledOnInCurrentSeason", (uint) 0);
			dbObj2.Set("survivedTimeInSurvival", (uint) 0);
			dbObj2.Set("lastMonthPlayed", (uint) DateTime.UtcNow.Month);
			dbObj2.Set("friends", new DatabaseArray());
			dbObj2.Set("otherPartyMembers", new DatabaseArray());
			NetworkManager.client.BigDB.CreateObject("PlayerObjects", LocalUserInfo.username, dbObj2, OnCreatePlayerDBObjectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnSaveDBObjectFail_Register (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
		}

		void OnCreatePlayerDBObjectSuccess_Register (DatabaseObject dbObj)
		{
			for (int i = 0; i < deactivateGosOnLogin.Length; i ++)
			{
				GameObject go = deactivateGosOnLogin[i];
				go.SetActive(false);
			}
			activateGoOnLogin.SetActive(true);
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
			rankedToggle.interactable = true;
			GameManager.updatables = GameManager.updatables.Add(LeaderboardMenu.resetLeaderboardUpdater);
		}

		void OnRegisterOrLoginFail (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
		}

		public void Login ()
		{
			rankedToggle.interactable = false;
			rankedToggle.isOn = false;
			if (string.IsNullOrEmpty(LocalUserInfo.username))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty username!");
				return;
			}
			else if (string.IsNullOrEmpty(password))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty password!");
				return;
			}
			Logout ();
			NetworkManager.Connect (OnConnectSuccess_Login, OnRegisterOrLoginFail);
		}

		public void OnConnectSuccess_Login (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			NetworkManager.client.BigDB.Load("PlayerObjects", LocalUserInfo.username, OnLoadPlayerDBObjectSuccess_Login, OnRegisterOrLoginFail);
		}

		void OnLoadPlayerDBObjectSuccess_Login (DatabaseObject dbObj)
		{
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
			if (dbObj.GetString("password") != password)
			{
				GameManager.instance.DisplayNotification ("Incorrect login info.");
				return;
			}
			DatabaseArray dbArray = dbObj.GetArray("skill");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.skillDict[gameModeType.name] = dbArray.GetFloat(gameModeType.name);
			}
			dbArray = dbObj.GetArray("skillInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.skillInCurrentSeasonDict[gameModeType.name] = dbArray.GetFloat(gameModeType.name);
			}
			dbArray = dbObj.GetArray("wins");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.winsDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("winsInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.winsInCurrentSeasonDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("losses");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.lossesDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("lossesInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.lossesInCurrentSeasonDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("kills");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.killsDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("killsInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.killsInCurrentSeasonDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("deaths");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.deathsDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			dbArray = dbObj.GetArray("deathsInCurrentSeason");
			for (int i = 0; i < LeaderboardMenu._gameModeTypes.Length; i ++)
			{
				LeaderboardMenu.GameModeType gameModeType = LeaderboardMenu._gameModeTypes[i];
				LocalUserInfo.deathsInCurrentSeasonDict[gameModeType.name] = dbArray.GetUInt(gameModeType.name);
			}
			LocalUserInfo.goalsMade = dbObj.GetUInt("goalsMade");
			LocalUserInfo.goalsMadeInCurrentSeason = dbObj.GetUInt("goalsMadeInCurrentSeason");
			LocalUserInfo.goaledOn = dbObj.GetUInt("goaledOn");
			LocalUserInfo.goaledOnInCurrentSeason = dbObj.GetUInt("goaledOnInCurrentSeason");
			LocalUserInfo.lastMonthPlayed = dbObj.GetUInt("lastMonthPlayed");
			DatabaseArray friendsDBArray = dbObj.GetArray("friends");
			FriendsMenu.friendsUsernames.Clear();
			for (int i = 0; i < friendsDBArray.Count; i ++)
			{
				string friend = friendsDBArray.GetString(i);
				FriendsMenu.friendsUsernames.Add(friend);
			}
			for (int i = 0; i < deactivateGosOnLogin.Length; i ++)
			{
				GameObject go = deactivateGosOnLogin[i];
				go.SetActive(false);
			}
			activateGoOnLogin.SetActive(true);
			rankedToggle.interactable = true;
			GameManager.updatables = GameManager.updatables.Add(LeaderboardMenu.resetLeaderboardUpdater);
		}

		void Logout ()
		{
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = false;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = false;
			}
			FriendsMenu.friendsUsernames.Clear();
			GameManager.updatables = GameManager.updatables.Remove(LeaderboardMenu.resetLeaderboardUpdater);
			NetworkManager.Disconnect ();
		}

		string GetInputFieldText (TMP_InputField inputField)
		{
			string output = inputField.text;
			if (string.IsNullOrEmpty(output))
				output = ((TMP_Text) inputField.placeholder).text;
			return output;
		}
	}
}