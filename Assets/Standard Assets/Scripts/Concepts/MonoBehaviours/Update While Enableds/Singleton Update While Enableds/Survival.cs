using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace BombGame
{
	public class Survival : SingletonUpdateWhileEnabled<Survival>
	{
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public AnimationCurve difficultyCurve;
		public FloatRange enemySpawnIntervalRange;
		public Transform[] enemySpawnPoints = new Transform[0];
		public float minDistanceFromPlayer;
		public Transform playerSpawnPoint;
		public _Text scoreText;
		public _Text bestScoreText;
		public float deathPenalty;
		public static int BestScore
		{
			get
			{
				return PlayerPrefs.GetInt("Survival best score", 0);
			}
			set
			{
				PlayerPrefs.SetInt("Survival best score", value);
			}
		}
		float score;
		float enemySpawnTimer;
		float difficulty;
		float previousDifficultyCurveValue;

		public override void OnEnable ()
		{
			base.OnEnable ();
			if (BestScore > 0)
			{
				float initScore = Mathf.Clamp(BestScore - deathPenalty, 0, Mathf.Infinity);
				bestScoreText.Text = "Best score: " + BestScore;
			}
		}

		public override void DoUpdate ()
		{
			score += Time.deltaTime;
			scoreText.Text = "" + (int) score;
			enemySpawnTimer -= Time.deltaTime;
			if (enemySpawnTimer <= 0)
			{
				float difficultyCurveValue = difficultyCurve.Evaluate(score);
				difficulty += difficultyCurveValue - previousDifficultyCurveValue;
				previousDifficultyCurveValue = difficultyCurveValue;
				SpawnEnemies ();
				enemySpawnTimer += enemySpawnIntervalRange.Get(Random.value);
			}
		}

		public async void OnPlayerDeath ()
		{
			if (score > BestScore)
				BestScore = (int) score;
			score = Mathf.Clamp(BestScore - deathPenalty, 0, Mathf.Infinity);
			previousDifficultyCurveValue = 0;
			enemySpawnTimer = 0;
			Enemy.instances = FindObjectsOfType<Enemy>(true);
			for (int i = 0; i < Enemy.instances.Length; i ++)
			{
				Enemy enemy = Enemy.instances[i];
				Destroy(enemy.gameObject);
			}
			Player.instance.trs.position = playerSpawnPoint.position;
			bestScoreText.Text = "Best score: " + BestScore;
			StartCoroutine(OnPlayerDeathRoutine ());
		}

		IEnumerator OnPlayerDeathRoutine ()
		{
			yield return new WaitForEndOfFrame();
			Player.instance.invulnerable = false;
		}

		void SpawnEnemies ()
		{
			List<EnemySpawnEntry> enemiesRemaining = new List<EnemySpawnEntry>(enemySpawnEntries);
			while (enemiesRemaining.Count > 0)
			{
				int randomIndex = Random.Range(0, enemiesRemaining.Count);
				EnemySpawnEntry enemySpawnEntry = enemySpawnEntries[randomIndex];
				if (enemySpawnEntry.difficulty > difficulty)
					enemiesRemaining.RemoveAt(randomIndex);
				else
				{
					Vector2 spawnPosition = new Vector2();
					List<Transform> enemySpawnPointsRemaining = new List<Transform>(enemySpawnPoints);
					Enemy enemyPrefab = enemySpawnEntry.enemyPrefab;
					do
					{
						int randomIndex2 = Random.Range(0, enemySpawnPointsRemaining.Count);
						spawnPosition = enemySpawnPointsRemaining[randomIndex2].position;
						enemySpawnPointsRemaining.RemoveAt(randomIndex2);
					} while (((Vector2) Player.instance.trs.position - spawnPosition).sqrMagnitude < minDistanceFromPlayer * minDistanceFromPlayer || Physics2D.OverlapCircle(spawnPosition, enemyPrefab.trs.lossyScale.x / 2, LayerMask.GetMask("Enemy")) != null);
					Instantiate(enemyPrefab, spawnPosition, enemyPrefab.trs.rotation);
					difficulty -= enemySpawnEntry.difficulty;
				}
			}
		}

		[Serializable]
		public class EnemySpawnEntry
		{
			public Enemy enemyPrefab;
			public float difficulty;
		}
	}
}