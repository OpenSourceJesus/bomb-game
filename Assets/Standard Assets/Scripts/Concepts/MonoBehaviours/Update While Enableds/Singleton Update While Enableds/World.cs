using Extensions;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BombGame
{
	public class World : SingletonUpdateWhileEnabled<World>
	{
		public ObjectInWorld[] worldObjects;
		public Vector2Int sizeOfPieces;
		public WorldPiece piecePrefab;
		public Dictionary<Vector2Int, WorldPiece> piecesDict = new Dictionary<Vector2Int, WorldPiece>();
		public WorldPiece[,] pieces;
		public Vector2Int maxPieceLocation;
		public Transform piecesParent;
		public RectInt cellBoundsRect;
		public Rect worldBoundsRect;
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		public Vector2Int loadPiecesRange;
		public List<WorldPiece> activePieces = new List<WorldPiece>();
		public Transform[] extraTransforms = new Transform[0];
#if UNITY_EDITOR
		public Transform initExtraTransformsParent;
		public Tilemap[] tilemaps = new Tilemap[0];
		public TileBase[] groundTiles = new TileBase[0];
		public TileBase[] hazardTiles = new TileBase[0];
		public Tilemap devInfoTilemap;
		public TileBase devInfoTile;
		public Vector2Int[] relativeCellPositionsAbovePlayerCanCrashIntoIfJumping = new Vector2Int[0];
		public uint playerVelocitySlices;
		public Vector2 playerPositionOffset;
		public bool update;
#endif
		WorldPiece piecePlayerIsIn;

		void Start ()
		{
// #if UNITY_EDITOR
// 			WorldMakerWindow.SetWorldActive (false);
// #endif
			SetPieces (true);
			Enemy.instances = FindObjectsOfType<Enemy>(true);
			Toggleable.instances = FindObjectsOfType<Toggleable>(true);
			ShooterTrap.instances = FindObjectsOfType<ShooterTrap>(true);
			ActivaterTile.instances = FindObjectsOfType<ActivaterTile>(true);
		}

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (!update)
				return;
			update = false;
			worldObjects = FindObjectsOfType<ObjectInWorld>();
			for (int i = 0; i < worldObjects.Length; i ++)
			{
				ObjectInWorld worldObject = worldObjects[i];
				if (worldObject.trs == null)
					MonoBehaviour.print(worldObject);
				if (!worldObject.enabled || (worldObject.trs.parent != null && worldObject.trs.parent.GetComponent<ObjectInWorld>() != null))
				{
					worldObjects = worldObjects.RemoveAt(i);
					i --;
				}
			}
			for (int i = 0; i < extraTransforms.Length; i ++)
			{
				Transform extraTrs = extraTransforms[i];
				extraTrs.SetParent(initExtraTransformsParent);
			}
			cellBoundsRect = new RectInt();
			cellBoundsRect.min = Vector2Int.zero;
			cellBoundsRect.max = Vector2Int.zero;
			Tilemap unexploredTilemap = WorldMap.Instance.unexploredTilemap;
			cellBoundsRect.SetMinMax(cellBoundsRect.min.SetToMinComponents(unexploredTilemap.cellBounds.min.ToVec2Int()), cellBoundsRect.max.SetToMaxComponents(unexploredTilemap.cellBounds.max.ToVec2Int()));
			Vector2 worldBoundsMin = unexploredTilemap.GetCellCenterWorld(cellBoundsRect.min.ToVec3Int()) - (unexploredTilemap.cellSize / 2);
			Vector2 worldBoundsMax = unexploredTilemap.GetCellCenterWorld(cellBoundsRect.max.ToVec3Int()) + (unexploredTilemap.cellSize / 2);
			worldBoundsRect = Rect.MinMaxRect(worldBoundsMin.x, worldBoundsMin.y, worldBoundsMax.x, worldBoundsMax.y);
			MarkCollidableHazardsAboveGroundWithoutUsingExplosions ();
		}

		void MarkCollidableHazardsAboveGroundWithoutUsingExplosions ()
		{
			List<Vector2Int> _relativeCellPositionsAbovePlayerCanCrashIntoIfJumping = new List<Vector2Int>();
			Vector2 initPosition = Vector2.one / 2 + Player.Instance.ColliderRect.size;
			Player.Instance.velocityEffectors_floatDict.Init ();
			for (int i = 0; i < playerVelocitySlices; i ++)
			{
				Vector2 position = initPosition;
				float xVelocity = Player.instance.velocityEffectors_floatDict["Move Speed"].value / (playerVelocitySlices - 1) * i;
				Vector2 velocity = new Vector2(xVelocity, Player.instance.velocityEffectors_floatDict["Jump Speed"].value);
				while (true)
				{
					velocity += Physics2D.gravity * Time.fixedDeltaTime;
					velocity.x = xVelocity;
					velocity *= 1f - Time.fixedDeltaTime * Player.instance.rigid.drag;
					position += velocity * Time.fixedDeltaTime;
					Vector2Int cellPosition = (position - initPosition + playerPositionOffset).ToVec2Int();
					// Vector2Int cellPosition = new Vector2Int(Mathf.RoundToInt(position.x - initPosition.x), Mathf.RoundToInt(position.y - initPosition.y));
					if (cellPosition.y > 0 && !_relativeCellPositionsAbovePlayerCanCrashIntoIfJumping.Contains(cellPosition))
						_relativeCellPositionsAbovePlayerCanCrashIntoIfJumping.Add(cellPosition);
					if (position.y <= initPosition.y)
						break;
				}
			}
			int _relativeCellPositionsAbovePlayerCanCrashIntoIfJumpingCount = _relativeCellPositionsAbovePlayerCanCrashIntoIfJumping.Count;
			for (int i = 0; i < _relativeCellPositionsAbovePlayerCanCrashIntoIfJumpingCount; i ++)
			{
				Vector2Int cellPosition = _relativeCellPositionsAbovePlayerCanCrashIntoIfJumping[i];
				if (cellPosition.x > 0)
				{
					cellPosition.x *= -1;
					_relativeCellPositionsAbovePlayerCanCrashIntoIfJumping.Add(cellPosition);
				}
			}
			relativeCellPositionsAbovePlayerCanCrashIntoIfJumping = _relativeCellPositionsAbovePlayerCanCrashIntoIfJumping.ToArray();
			devInfoTilemap.ClearAllTiles();
			for (int i = 0; i < tilemaps.Length; i ++)
			{
				Tilemap tilemap = tilemaps[i];
				foreach (Vector3Int cellPosition in tilemap.cellBounds.allPositionsWithin)
				{
					TileBase tile = tilemap.GetTile(cellPosition);
					if (groundTiles.Contains(tile))
					{
						for (int i2 = 0; i2 < relativeCellPositionsAbovePlayerCanCrashIntoIfJumping.Length; i2 ++)
						{
							Vector2Int relativeCellPosition = relativeCellPositionsAbovePlayerCanCrashIntoIfJumping[i2];
							TileBase tile2 = tilemap.GetTile(cellPosition + (Vector3Int) relativeCellPosition);
							if (hazardTiles.Contains(tile2))
								devInfoTilemap.SetTile(cellPosition + (Vector3Int) relativeCellPosition, devInfoTile);
						}
					}
				}
			}
		}
#endif

		public virtual void SetPieces (bool parentExtraTransforms)
		{
			pieces = new WorldPiece[maxPieceLocation.x + 1, maxPieceLocation.y + 1];
			piecesDict.Clear();
			piecePlayerIsIn = null;
			List<Transform> _extraTransforms = new List<Transform>(extraTransforms);
			for (int i = 0; i < piecesParent.childCount; i ++)
			{
				WorldPiece piece = piecesParent.GetChild(i).GetComponent<WorldPiece>();
				piecesDict.Add(piece.location, piece);
				pieces[piece.location.x, piece.location.y] = piece;
				if (piecePlayerIsIn == null && piece.worldBoundsRect.Contains(Player.Instance.trs.position))
					piecePlayerIsIn = piece;
				if (parentExtraTransforms)
				{
					for (int i2 = 0; i2 < _extraTransforms.Count; i2 ++)
					{
						Transform extraTrs = _extraTransforms[i2];
						if (piece.worldBoundsRect.Contains(extraTrs.position))
						{
							extraTrs.SetParent(piece.trs);
							_extraTransforms.RemoveAt(i2);
							i2 --;
						}
					}
				}
			}
		}

		public override void DoUpdate ()
		{
			if (WorldMap.isOpen)
				return;
			List<WorldPiece> previousActivePieces = new List<WorldPiece>(activePieces);
			activePieces.Clear();
			if (!piecePlayerIsIn.worldBoundsRect.Contains(Player.Instance.trs.position))
			{
				WorldPiece[] surroundingPieces = GetSurroundingPieces(piecePlayerIsIn);
				for (int i = 0; i < surroundingPieces.Length; i ++)
				{
					WorldPiece surroundingPiece = surroundingPieces[i];
					if (surroundingPiece.worldBoundsRect.Contains(Player.instance.trs.position))
					{
						piecePlayerIsIn = surroundingPiece;
						break;
					}
				}
			}
			activePieces.Add(piecePlayerIsIn);
			piecePlayerIsIn.gameObject.SetActive(true);
			WorldPiece[] surroundingPieces2 = GetSurroundingPieces(piecePlayerIsIn);
			for (int i = 0; i < surroundingPieces2.Length; i ++)
			{
				WorldPiece surroundingPiece = surroundingPieces2[i];
				Rect loadPieceRangeRect = surroundingPiece.worldBoundsRect.Expand(loadPiecesRange * 2);
				if (GameCamera.Instance.viewRect.IsIntersecting(loadPieceRangeRect))
				{
					activePieces.Add(surroundingPiece);
					surroundingPiece.gameObject.SetActive(true);
					for (int i2 = 0; i2 < surroundingPiece.piecesToLoadAndUnloadWithMe.Length; i2 ++)
					{
						WorldPiece worldPiece = surroundingPiece.piecesToLoadAndUnloadWithMe[i2];
						worldPiece.gameObject.SetActive(true);
						activePieces.Add(worldPiece);
					}
				}
				else
				{
					activePieces.Remove(surroundingPiece);
					surroundingPiece.gameObject.SetActive(false);
				}
			}
			for (int i = 0; i < piecePlayerIsIn.piecesToLoadAndUnloadWithMe.Length; i ++)
			{
				WorldPiece worldPiece = piecePlayerIsIn.piecesToLoadAndUnloadWithMe[i];
				worldPiece.gameObject.SetActive(true);
				activePieces.Add(worldPiece);
			}
			for (int i = 0; i < previousActivePieces.Count; i ++)
			{
				WorldPiece previousActivePiece = previousActivePieces[i];
				if (!activePieces.Contains(previousActivePiece))
					previousActivePiece.gameObject.SetActive(false);
			}
		}

		public virtual WorldPiece[] GetSurroundingPieces (params WorldPiece[] innerPieces)
		{
			List<WorldPiece> output = new List<WorldPiece>();
			for (int i = 0; i < innerPieces.Length; i ++)
			{
				WorldPiece piece = innerPieces[i];
				bool hasPieceRight = piece.location.x < maxPieceLocation.x;
				bool hasPieceLeft = piece.location.x > 0;
				bool hasPieceUp = piece.location.y < maxPieceLocation.y;
				bool hasPieceDown = piece.location.y > 0;
				if (hasPieceUp)
				{
					output.Add(pieces[piece.location.x, piece.location.y + 1]);
					if (hasPieceRight)
						output.Add(pieces[piece.location.x + 1, piece.location.y + 1]);
					if (hasPieceLeft)
						output.Add(pieces[piece.location.x - 1, piece.location.y + 1]);
				}
				if (hasPieceDown)
				{
					output.Add(pieces[piece.location.x, piece.location.y - 1]);
					if (hasPieceRight)
						output.Add(pieces[piece.location.x + 1, piece.location.y - 1]);
					if (hasPieceLeft)
						output.Add(pieces[piece.location.x - 1, piece.location.y - 1]);
				}
				if (hasPieceRight)
					output.Add(pieces[piece.location.x + 1, piece.location.y]);
				if (hasPieceLeft)
					output.Add(pieces[piece.location.x - 1, piece.location.y]);
				for (int i2 = 0; i2 < innerPieces.Length; i2 ++)
				{
					WorldPiece piece2 = innerPieces[i2];
					output.Remove(piece2);
				}
			}
			return output.ToArray();
		}
	}
}