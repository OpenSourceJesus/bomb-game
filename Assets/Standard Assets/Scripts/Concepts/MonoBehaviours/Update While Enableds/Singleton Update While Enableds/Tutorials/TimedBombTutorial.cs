using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BombGame
{
	public class TimedBombTutorial : Tutorial
	{
		public GameObject activateOnUnequipTimedBomb;
		public GameObject activateOnEquipTimedBomb;
		public GameObject deactivateOnEquipTimedBomb;
		public GameObject activateOnArmTimedBomb;
		public GameObject activateOnThrowTimedBomb;
		public PlayerRecording playerRecording;
		public BoxCollider2D canStartPlayerRecordingInsideBoxCollider;
		Bomb armedTimedBomb;
		bool equipedTimedBomb;
		bool didArmTimedBomb;

		public override void DoUpdate ()
		{
			if (!canStartPlayerRecordingInsideBoxCollider.bounds.ToRect().Contains(Player.Instance.trs.position))
				return;
			if (activateOnThrowTimedBomb.activeSelf)
			{
				if ((InputManager.UsingKeyboard && Keyboard.current.hKey.wasPressedThisFrame) || (InputManager.UsingGamepad && Gamepad.current.yButton.wasPressedThisFrame))
				{
					playerRecording.enabled = false;
					playerRecording.currentKeyframeIndex = 0;
					playerRecording.timer = 0;
					playerRecording.enabled = true;
				}
			}
			else
			{
				if (Player.instance.currentBomb.explodeOnHit)
				{
					if (equipedTimedBomb)
					{
						GameManager.ActivateGameObjectForever (activateOnUnequipTimedBomb);
						GameManager.DeactivateGameObjectForever (activateOnEquipTimedBomb);
#if !UNITY_WEBGL
						SaveAndLoadManager.Save ();
#endif
					}
				}
				else
				{
					if (Player.instance.armedBombNamesDict.Count > 0)
					{
						armedTimedBomb = Player.instance.GetComponentInChildren<Bomb>();
						if (!didArmTimedBomb)
						{
							GameManager.ActivateGameObjectForever (activateOnArmTimedBomb);
#if !UNITY_WEBGL
						SaveAndLoadManager.Save ();
#endif
						}
						didArmTimedBomb = true;
					}
					else if (armedTimedBomb != null && armedTimedBomb.canExplode)
					{
						if (!activateOnThrowTimedBomb.activeSelf)
						{
							GameManager.DeactivateGameObjectForever (activateOnArmTimedBomb);
							GameManager.ActivateGameObjectForever (activateOnThrowTimedBomb);
#if !UNITY_WEBGL
						SaveAndLoadManager.Save ();
#endif
						}
					}
					else if (!didArmTimedBomb)
					{
						GameManager.DeactivateGameObjectForever (activateOnUnequipTimedBomb);
						GameManager.ActivateGameObjectForever (activateOnEquipTimedBomb);
						GameManager.DeactivateGameObjectForever (deactivateOnEquipTimedBomb);
#if !UNITY_WEBGL
						SaveAndLoadManager.Save ();
#endif
						equipedTimedBomb = true;
					}
				}
			}
		}
	}
}