using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	public class GameCamera : CameraScript
	{
		public new static GameCamera instance;
		public new static GameCamera Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<GameCamera>(true);
				return instance;
			}
		}
		public bool followPlayer;
		
		public override void Awake ()
		{
			Player.instance = Player.Instance;
			base.Awake ();
		}

		public override void HandlePosition ()
		{
			if (followPlayer && Player.instance.polygonCollider != null)
            	trs.position = Player.instance.polygonCollider.bounds.center.SetZ(trs.position.z);
            base.HandlePosition ();
		}
	}
}