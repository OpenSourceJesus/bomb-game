using System;
using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Collections.Generic;

namespace BombGame
{
	public class Soccer : Match
	{
		public SerializableDictionary<Team, SoccerGoal> teamGoalsDict = new SerializableDictionary<Team, SoccerGoal>();
		public SerializableDictionary<Team, SpawnPointGroup> teamSpawnTransformsDict = new SerializableDictionary<Team, SpawnPointGroup>();
		public float countdownDuration;
		public _Text countdownText;
		public Transform countdownBar;
		public Team[] teams = new Team[0];
		public new static Soccer instance;
		public new static Soccer Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Soccer>();
				return instance;
			}
			set
			{
				instance = value;
			}
		}
		Vector2 ballSpawnPosition;

		public override void Awake ()
		{
			base.Awake ();
			teamGoalsDict.Init ();
			teamSpawnTransformsDict.Init ();
			ballSpawnPosition = Soccerball.instance.trs.position;
		}

		public override Player OnSpawnPlayerMessage (Message message)
		{
			Player player = base.OnSpawnPlayerMessage(message);
			Scoreboard.instance.RemoveEntry (player.owner);
			player.owner = teams[player.id % 2];
			ApplyTeam (player);
			Scoreboard.instance.AddEntry (player.owner);
			return player;
		}

		public void OnGoalHit (SoccerGoal goal)
		{
			Match.Team goalingTeam;
			Match.Team victimTeam = goal.owner;
			if (goal.owner == playerTeamsDict.Values.Get(0))
				goalingTeam = playerTeamsDict.Values.Get(1);
			else
				goalingTeam = playerTeamsDict.Values.Get(0);
			goalingTeam.score ++;
			Scoreboard.instance.UpdateEntry (goalingTeam);
			if (ranked && localPlayer.owner == goalingTeam)
			{
				LocalUserInfo.goalsMade ++;
				LocalUserInfo.goalsMadeInCurrentSeason ++;
			}
			else if (localPlayer.owner == victimTeam)
			{
				LocalUserInfo.goaledOn ++;
				LocalUserInfo.goaledOnInCurrentSeason ++;
			}
			Transform[] teamSpawnTransforms = teamSpawnTransformsDict[goalingTeam].transforms;
			for (int i = 0; i < goalingTeam.representatives.Length; i ++)
			{
				Player player = goalingTeam.representatives[i];
				player.trs.position = teamSpawnTransforms[i % teamSpawnTransforms.Length].position;
			}
			teamSpawnTransforms = teamSpawnTransformsDict[victimTeam].transforms;
			for (int i = 0; i < victimTeam.representatives.Length; i ++)
			{
				Player player = victimTeam.representatives[i];
				player.trs.position = teamSpawnTransforms[i % teamSpawnTransforms.Length].position;
			}
			Soccerball.instance.trs.position = ballSpawnPosition;
			GameManager.updatables = GameManager.updatables.Add(new CountdownUpdater());
			if (!hasEnded && goalingTeam.score >= maxScore)
			{
				if (ranked)
				{
					if (localPlayer.owner == goalingTeam)
					{
						float skillChange = 1f - ((float) victimTeam.score / maxScore);
						LocalUserInfo.skillDict[name] += skillChange;
						LocalUserInfo.skillInCurrentSeasonDict[name] ++;
						LocalUserInfo.winsDict[name] ++;
						LocalUserInfo.winsInCurrentSeasonDict[name] ++;
					}
					else
					{
						float skillChange = 1f - ((float) localPlayer.owner.score / maxScore);
						LocalUserInfo.skillDict[name] -= skillChange;
						LocalUserInfo.skillInCurrentSeasonDict[name] -= skillChange;
						LocalUserInfo.lossesDict[name] ++;
						LocalUserInfo.lossesInCurrentSeasonDict[name] ++;
					}
				}
				End ();
			}
		}

		[Serializable]
		public class SpawnPointGroup
		{
			public Transform[] transforms = new Transform[0];
		}

		class CountdownUpdater : IUpdatable
		{
			float timer;

			public CountdownUpdater ()
			{
				Soccer.instance.countdownBar.gameObject.SetActive(true);
			}

			public void DoUpdate ()
			{
				timer += Time.deltaTime;
				Soccer.instance.countdownBar.localScale = Soccer.instance.countdownBar.localScale.SetX(Soccer.instance.countdownDuration - timer / Soccer.instance.countdownDuration);
				if (timer < Soccer.instance.countdownDuration / 3)
					Soccer.instance.countdownText.Text = "Ready...";
				else if (timer < Soccer.instance.countdownDuration / 3 * 2)
					Soccer.instance.countdownText.Text = "Set...";
				else
				{
					Soccer.instance.countdownText.Text = "Go!";
					if (timer >= Soccer.instance.countdownDuration)
					{
						Soccer.instance.countdownBar.gameObject.SetActive(false);
						GameManager.updatables = GameManager.updatables.Remove(this);
					}
				}
			}
		}
	}
}