using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace BombGame
{
	public class AffectedByRift : MonoBehaviour, IUpdatable
	{
		public static List<AffectedByRift> instances = new List<AffectedByRift>();
		public MeshFilter meshFilter;
		public Collider2D previousCollider;
		public PolygonCollider2D polygonCollider;
		public Shape2D shape;
		public Transform trs;
		public bool riftKillsMe;
		IDestructable destructable;
		Vector2 shapeCenter;
		Rect boundsRect;
		bool isRiftOnLeft;
		bool enableCollider;
		bool previousTouchingRift;
		Vector2 closestRiftPoint;
		Vector2 addToOffset;

		void OnEnable ()
		{
			instances.Add(this);
			if (riftKillsMe)
				destructable = GetComponent<IDestructable>();
		}

		void OnDisable ()
		{
			instances.Remove(this);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (!riftKillsMe)
			{
				if (meshFilter == null)
					meshFilter = GetComponent<MeshFilter>();
				if (polygonCollider == null)
					polygonCollider = GetComponent<PolygonCollider2D>();
				if (shape.corners.Length == 0)
				{
					if (polygonCollider != null)
						shape = new Shape2D(polygonCollider.points);
					else
						shape = new Shape2D(new Vector2[] { Vector2.one / 2, new Vector2(1, -1) / 2, -Vector2.one / 2, new Vector2(-1, 1) / 2 });
				}
			}
			else if (trs == null)
				trs = GetComponent<Transform>();
		}
#endif
		
		public void Enable ()
		{
			bool previousColliderEnabled = previousCollider.enabled;
			previousCollider.enabled = true;
			boundsRect = previousCollider.bounds.ToRect();
			previousCollider.enabled = previousColliderEnabled;
			closestRiftPoint = RiftBomb.riftCenterLineSegment.GetClosestPoint(boundsRect.center);
			addToOffset = closestRiftPoint - boundsRect.center;
			if (Vector2.Angle(addToOffset, RiftBomb.riftVector) < 90)
				addToOffset = RiftBomb.riftVector;
			else
				addToOffset = -RiftBomb.riftVector;
			if (!riftKillsMe && RiftBomb.riftState == RiftBomb.State.Opening)
			{
				isRiftOnLeft = addToOffset == RiftBomb.riftVector;
				enableCollider = true;
				if (meshFilter != null)
					meshFilter.mesh.bounds = new Bounds(CameraScript.instance.trs.position, Vector3.one * 99999);
			}
			if (RiftBomb.riftState == RiftBomb.State.Closing)
				addToOffset *= -1;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void Disable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			if (RiftBomb.riftState == RiftBomb.State.Disabled)
			{
				if (!riftKillsMe)
				{
					previousCollider.offset = Vector2.zero;
					previousCollider.enabled = true;
					polygonCollider.enabled = false;
				}
				bool previousColliderEnabled = previousCollider.enabled;
				previousCollider.enabled = true;
				boundsRect = previousCollider.bounds.ToRect();
				previousCollider.enabled = previousColliderEnabled;
			}
		}

		public void DoUpdate ()
		{
			if (!riftKillsMe)
			{
				shape = shape.Move(boundsRect.center - shapeCenter);
				shapeCenter = boundsRect.center;
				bool previousColliderEnabled = previousCollider.enabled;
				previousCollider.enabled = true;
				boundsRect = previousCollider.bounds.ToRect();
				previousCollider.enabled = previousColliderEnabled;
				previousCollider.offset += addToOffset * Time.deltaTime;
				previousCollider.enabled = enableCollider;
				polygonCollider.enabled = false;
				bool touchingRift = boundsRect.Contains(closestRiftPoint);
				if (touchingRift)
				{
					Shape2D trimmedShape = shape.Trim_ConvexPolygon(RiftBomb.riftCenterLineSegment, isRiftOnLeft);
					polygonCollider.points = trimmedShape.corners;
					polygonCollider.offset = -trs.position;
					polygonCollider.enabled = enableCollider;
					previousCollider.enabled = false;
				}
				else if (previousTouchingRift)
					enableCollider = RiftBomb.riftState == RiftBomb.State.Closing;
				previousTouchingRift = touchingRift;
			}
			else
			{
				trs.position += (Vector3) addToOffset * Time.deltaTime;
				if (destructable != null)
				{
					boundsRect = previousCollider.bounds.ToRect();
					closestRiftPoint = RiftBomb.riftCenterLineSegment.GetClosestPoint(boundsRect.center);
					if (previousCollider.OverlapPoint(closestRiftPoint))
						destructable.Death (null);
				}
			}
		}

		void OnDestroy ()
		{
			instances.Remove(this);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}