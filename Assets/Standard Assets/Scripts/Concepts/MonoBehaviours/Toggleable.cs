using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BombGame
{
	[ExecuteInEditMode]
	public class Toggleable : MonoBehaviour
	{
		// [HideInInspector]
		public bool initActive;
		public bool active;
		public Renderer renderer;
		public Collider2D collider;
		public ToggleBehaviour toggleBehaviour;
		Toggleable[] toggleChildren = new Toggleable[0];
		public static Toggleable[] instances = new Toggleable[0];

		void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (renderer == null)
					renderer = GetComponent<Renderer>();
				if (collider == null)
					collider = GetComponent<Collider2D>();
				return;
			}
#endif
			initActive = active;
			toggleChildren = GetComponentsInChildren<Toggleable>();
			for (int i = 1; i < toggleChildren.Length; i ++)
			{
				Toggleable toggle = toggleChildren[i];
				toggle.initActive = active;
				if (toggle.active != active)
					toggle.Toggle ();
			}
		}

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (Application.isPlaying)
				return;
			if (collider != null && collider.enabled != active)
			{
				active = !active;
				Toggle ();
			}
		}
#endif

		public void Toggle ()
		{
			active = !active;
			for (int i = 1; i < toggleChildren.Length; i ++)
			{
				Toggleable toggle = toggleChildren[i];
				if (toggle.active != active)
					toggle.Toggle ();
			}
			if (toggleBehaviour != ToggleBehaviour.OnlyToggleChildren)
			{
				collider.enabled = active;
				if (active)
				{
					if (toggleBehaviour == ToggleBehaviour.RendererColor)
					{
						SpriteRenderer spriteRenderer = (SpriteRenderer) renderer;
						spriteRenderer.color = spriteRenderer.color.MultiplyAlpha(4);
					}
					else// if (toggleBehaviour == ToggleBehaviour.MaterialTint)
						renderer.material.SetColor("_tint", renderer.material.GetColor("_tint").MultiplyAlpha(4));
				}
				else
				{
					if (toggleBehaviour == ToggleBehaviour.RendererColor)
					{
						SpriteRenderer spriteRenderer = (SpriteRenderer) renderer;
						spriteRenderer.color = spriteRenderer.color.DivideAlpha(4);
					}
					else// if (toggleBehaviour == ToggleBehaviour.MaterialTint)
						renderer.material.SetColor("_tint", renderer.material.GetColor("_tint").DivideAlpha(4));
				}
			}
		}

		public enum ToggleBehaviour
		{
			RendererColor,
			MaterialTint,
			OnlyToggleChildren
		}
	}
}