using System;
using UnityEngine;

namespace BombGame
{
	[Serializable]
	public struct AnimationEntry
	{
		public string animatorStateName;
		public int layer;
		public Animator animator;

		public void Play ()
		{
			animator.Play(animatorStateName, layer);
		}

		public bool IsPlaying ()
		{
			return animator.GetCurrentAnimatorStateInfo(layer).IsName(animatorStateName);
		}
	}
}