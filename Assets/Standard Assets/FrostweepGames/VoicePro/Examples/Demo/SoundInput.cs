﻿using FrostweepGames.Plugins.Native;
using FrostweepGames.VoicePro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace FrostweepGames.VoicePro.Examples
{
    public class SoundInput : MonoBehaviour
    {
        public Recorder recorder;

        public Listener listener;

        public bool isRecording;
        bool recordInput;
        bool previousRecordInput;

        void Update()
        {
            recordInput = Keyboard.current.rKey.isPressed;
            if (recordInput && !previousRecordInput && !isRecording)
            {
                StartRecord();
            }
            else if (!recordInput && previousRecordInput && isRecording)
            {
                StopRecord();
            }
            previousRecordInput = recordInput;
        }

        public void StartRecord()
        {
            if (CustomMicrophone.HasConnectedMicrophoneDevices())
            {
                recorder.SetMicrophone(CustomMicrophone.devices[0]);
                isRecording = recorder.StartRecord();

                Debug.Log("Record started: " + isRecording);
            }
			else
			{
                recorder.RefreshMicrophones();
            }  
        }

        public void StopRecord()
        {
            isRecording = false;
            recorder.StopRecord();

            Debug.Log("Record ended");
        }
    }
}