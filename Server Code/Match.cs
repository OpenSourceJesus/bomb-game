using System;
using PlayerIO.GameLibrary;
using System.Collections.Generic;

public class Match
{
	public virtual string DatabaseEntryPrefix
	{
		get
		{
			throw new NotImplementedException();
		}
	}
	float makeVulnerableDelay;
	uint minPlayers;
	uint maxPlayers;
	uint spawnPointCount;
	uint minTimeWithNoScoreChangeToEndRound;
	bool ranked;
	byte maxScore;
	List<uint> spawnPointsRemaining = new List<uint>();
	uint[] spawnPoints = new uint[0];
	Game<Player> game;
	List<uint> unusedPlayerIds = new List<uint>();
	Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();
	List<Player> allPlayers = new List<Player>();
	Timer endRoundTimer;
	uint currentRound;
	bool hasEnded;
	Random random = new Random();
	const uint MAX_COLOR_TRIES = 10;
	const float MIN_COLOR_SIMULARITY = .15f;

	public Match (Game<Player> game)
	{
		this.game = game;
		makeVulnerableDelay = float.Parse(game.RoomData["makeVulnerableDelay"]);
		minPlayers = uint.Parse(game.RoomData["minPlayers"]);
		maxPlayers = uint.Parse(game.RoomData["maxPlayers"]);
		for (uint i = 0; i < maxPlayers; i ++)
			unusedPlayerIds.Add(i);
		spawnPointCount = uint.Parse(game.RoomData["spawnPointCount"]);
		spawnPoints = new uint[spawnPointCount];
		for (uint i = 0; i < spawnPointCount; i ++)
			spawnPoints[i] = i;
		spawnPointsRemaining = new List<uint>(spawnPoints);
		minTimeWithNoScoreChangeToEndRound = uint.Parse(game.RoomData["minTimeWithNoScoreChangeToEndRound"]);
		ranked = bool.Parse(game.RoomData["ranked"]);
		maxScore = byte.Parse(game.RoomData["maxScore"]);
		game.PreloadPlayerObjects = true;
	}

	public void UserJoined (Player player)
	{
		uint playerId = unusedPlayerIds[0];
		allPlayers.Add(player);
		foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
		{
			uint usedPlayerId = keyValuePair.Key;
			Player player2 = keyValuePair.Value;
			player.Send("Spawn Player", usedPlayerId, player2.color.x, player2.color.y, player2.color.z, (uint) 0);
			player.Send("Move Player", usedPlayerId, player2.position.x, player2.position.y);
			if (!player2.invulnerable)
				player.Send("Make Player Vulnerable", usedPlayerId);
		}
		int spawnPointIndex = random.Next(spawnPointsRemaining.Count);
		Vector3 teamColor = new Vector3();
		float leastColorSimularity = float.MaxValue;
		Vector3 leastSimularColor = new Vector3();
		bool isValidColor = true;
		for (int i = 0; i < MAX_COLOR_TRIES; i ++)
		{
			isValidColor = true;
			teamColor = new Vector3((float) random.NextDouble(), (float) random.NextDouble(), (float) random.NextDouble());
			foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
			{
				float colorSimularity = 1f - Vector3.GetDifference(keyValuePair.Value.color, teamColor);
				if (colorSimularity < MIN_COLOR_SIMULARITY)
				{
					if (colorSimularity < leastColorSimularity)
					{
						leastColorSimularity = colorSimularity;
						leastSimularColor = teamColor;
					}
					isValidColor = false;
					break;
				}
			}
			if (isValidColor)
				break;
		}
		if (!isValidColor)
			teamColor = leastSimularColor;
		player.color = teamColor;
		uint spawnPoint = spawnPointsRemaining[spawnPointIndex];
		Message spawnPlayerMessage = Message.Create("Spawn Player", playerId, teamColor.x, teamColor.y, teamColor.z, spawnPoint);
		Message spawnLocalPlayerMessage = Message.Create("Spawn Player", playerId, teamColor.x, teamColor.y, teamColor.z, spawnPoint, true);
		spawnPointsRemaining.RemoveAt(spawnPointIndex);
		foreach (Player player2 in game.Players)
		{
			if (player != player2)
				player2.Send(spawnPlayerMessage);
			else
			{
				player.Send(spawnLocalPlayerMessage);
				for (uint i = 0; i < currentRound; i ++)
					player.Send("End Round");
			}
		}
		unusedPlayerIds.RemoveAt(0);
		player.id = playerId;
		playersDict.Add(playerId, player);
		game.ScheduleCallback(() => { MakePlayerVulnerable (player); }, (int) (makeVulnerableDelay * 1000));
		if (game.PlayerCount >= Math.Max(minPlayers, 2))
		{
			spawnPointsRemaining = new List<uint>(spawnPoints);
			if (endRoundTimer == null)
				endRoundTimer = game.AddTimer(EndRound, (int) minTimeWithNoScoreChangeToEndRound * 1000);
		}
	}

	public void UserLeft (Player player)
	{
		playersDict.Remove(player.id);
		if (ranked && playersDict.Count == 1 && !hasEnded)
		{
			foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
				End (keyValuePair.Value);
		}
		unusedPlayerIds.Add(player.id);
		if (playersDict.Count < Math.Max(minPlayers, 2) && endRoundTimer != null)
		{
			endRoundTimer.Stop();
			endRoundTimer = null;
		}
		foreach (Player player2 in game.Players)
		{
			if (player != player2)
				player2.Send("Remove Player", player.id);
		}
	}

	public void GotMessage (Player player, Message message)
	{
		if (message.Type == "Move Player")
		{
			player.position = new Vector2(message.GetFloat(0), message.GetFloat(1));
			Message movePlayerMessage = Message.Create("Move Player", player.id, player.position.x, player.position.y);
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(movePlayerMessage);
			}
		}
		else if (message.Type == "Shoot Bomb")
		{
			Message shootBombMessage = Message.Create("Shoot Bomb", player.id, message.GetUInt(0), message.GetFloat(1), message.GetFloat(2));
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(shootBombMessage);
			}
		}
		else if (message.Type == "Revive Player")
		{
			Message revivePlayerMessage = Message.Create("Revive Player", player.id, message.GetUInt(0));
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(revivePlayerMessage);
			}
			player.invulnerable = true;
			game.ScheduleCallback(() => { MakePlayerVulnerable (player); }, (int) (makeVulnerableDelay * 1000));
		}
		else
		{
			message.Add(player.id);
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(message);
			}
			if (message.Type == "Kill Player")
			{
				uint killerId = message.GetUInt(0);
				Player killer = playersDict[killerId];
				if (killer != player)
				{
					killer.score ++;
					if (killer.score >= maxScore && !hasEnded)
						End (killer);
				}
				else
					player.score --;
				endRoundTimer.Stop();
				endRoundTimer = game.AddTimer(EndRound, (int) minTimeWithNoScoreChangeToEndRound * 1000);
				if (ranked)
				{
					DatabaseObject victimDBObject = player.PlayerObject;
					victimDBObject.Set(DatabaseEntryPrefix + "Deaths", victimDBObject.GetUInt(DatabaseEntryPrefix + "Deaths") + 1);
					victimDBObject.Set(DatabaseEntryPrefix + "DeathsInCurrentSeason", victimDBObject.GetUInt(DatabaseEntryPrefix + "DeathsInCurrentSeason") + 1);
					victimDBObject.Save();
					DatabaseObject killerDBObject = killer.PlayerObject;
					killerDBObject.Set(DatabaseEntryPrefix + "Kills", killerDBObject.GetUInt(DatabaseEntryPrefix + "Kills") + 1);
					killerDBObject.Set(DatabaseEntryPrefix + "KillsInCurrentSeason", killerDBObject.GetUInt(DatabaseEntryPrefix + "KillsInCurrentSeason") + 1);
					killerDBObject.Save();
				}
			}
		}
	}

	void MakePlayerVulnerable (Player player)
	{
		player.invulnerable = false;
		foreach (Player player2 in game.Players)
			player2.Send("Make Player Vulnerable", player.id);
	}

	void End (Player winner)
	{
		hasEnded = true;
		if (ranked)
		{
			List<Player> losingPlayers = new List<Player>(allPlayers);
			losingPlayers.Remove(winner);
			for (int i = 0; i < losingPlayers.Count; i ++)
			{
				Player loser = losingPlayers[i];
				DatabaseObject loserDBObject = loser.PlayerObject;
				loserDBObject.Set(DatabaseEntryPrefix + "Losses", loserDBObject.GetUInt(DatabaseEntryPrefix + "Losses") + 1);
				loserDBObject.Set(DatabaseEntryPrefix + "LossesInCurrentSeason", loserDBObject.GetUInt(DatabaseEntryPrefix + "LossesInCurrentSeason") + 1);
				float skillChange;
				if (loser.score >= 0)
					skillChange = 1f - ((float) loser.score / maxScore);
				else
					skillChange = 1f + ((float) Math.Abs(loser.score) / maxScore);
				loserDBObject.Set(DatabaseEntryPrefix + "Skill", loserDBObject.GetFloat(DatabaseEntryPrefix + "Skill") - skillChange);
				loserDBObject.Set(DatabaseEntryPrefix + "SkillInCurrentSeason", loserDBObject.GetFloat(DatabaseEntryPrefix + "SkillInCurrentSeason") - skillChange);
				loserDBObject.Save();
			}
			DatabaseObject winnerDBObject = winner.PlayerObject;
			winnerDBObject.Set(DatabaseEntryPrefix + "Wins", winnerDBObject.GetUInt(DatabaseEntryPrefix + "Wins") + 1);
			winnerDBObject.Set(DatabaseEntryPrefix + "WinsInCurrentSeason", winnerDBObject.GetUInt(DatabaseEntryPrefix + "WinsInCurrentSeason") + 1);
			winnerDBObject.Set(DatabaseEntryPrefix + "Skill", winnerDBObject.GetFloat(DatabaseEntryPrefix + "Skill") + 1);
			winnerDBObject.Set(DatabaseEntryPrefix + "SkillInCurrentSeason", winnerDBObject.GetFloat(DatabaseEntryPrefix + "SkillInCurrentSeason") + 1);
			winnerDBObject.Save();
		}
		DisconnectPlayers ();
	}

	void DisconnectPlayers ()
	{
		List<Player> players = new List<Player>(playersDict.Values);
		for (int i = 0; i < players.Count; i ++)
		{
			Player player = players[i];
			player.Disconnect();
		}
	}

	void EndRound ()
	{
		foreach (Player player in game.Players)
			player.Send("End Round");
		currentRound ++;
	}
}