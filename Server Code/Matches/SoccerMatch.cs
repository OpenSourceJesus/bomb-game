using PlayerIO.GameLibrary;

public class SoccerMatch : Match
{
    public override string DatabaseEntryPrefix => "soccer";

    public SoccerMatch (Game<Player> game) : base (game)
	{
	}
}