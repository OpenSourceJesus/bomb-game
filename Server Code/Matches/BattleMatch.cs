using PlayerIO.GameLibrary;

public class BattleMatch : Match
{
    public override string DatabaseEntryPrefix => "battle";

    public BattleMatch (Game<Player> game) : base (game)
	{
	}
}